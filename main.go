package main

import (
	"commerce/common"
	"commerce/routers"
	"github.com/codegangsta/negroni"
	"net/http"
)

// go build 不会把静态资源打包进去，所以把源代码通过gitee下载zip，上传到服务器
// CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build

// 在linux上运行好后，需要exit退出下，不能直接关闭secureCRT, 会让程序也退出

func main() {

	common.StartUp()
	router := routers.InitRoutes()
	//n := negroni.Classic()
	//n.UseHandler(router)
	// 减少 negroni 日志的打印
	recovery := negroni.NewRecovery()
	recovery.Logger = common.Error
	n := negroni.New(recovery, negroni.Wrap(router))

	server := &http.Server{
		Addr:    common.AppConfig.Server,
		Handler: n,
	}

	common.Info.Println("Listening on:", server.Addr)
	server.ListenAndServe()
}
