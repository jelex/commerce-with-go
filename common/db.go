package common

import (
	"database/sql"
	"github.com/go-sql-driver/mysql"
)

var (
	SqlConfig mysql.Config
	DB        *sql.DB
	Err       error
)

func initDBConfig() {

	//cfg := mysql.Config{
	//	User:                 "root",
	//	Passwd:               "root",
	//	Net:                  "tcp",
	//	Addr:                 "localhost:3306",
	//	DBName:               "",
	//	AllowNativePasswords: true, // 需要这行配置
	//}
	DB, Err = sql.Open("mysql", AppConfig.SqlConfig.FormatDSN())

	if Err != nil {
		panic(Err)
	}
}
