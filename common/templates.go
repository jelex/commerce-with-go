package common

import (
	"html/template"
	"net/http"
	"os"
	"path/filepath"
)

var Templates = make(map[string]*template.Template)
var CurDir, _ = os.Getwd()

func init() {

	Templates["404"] = template.Must(template.ParseFiles(filepath.Join(CurDir, "views/404.html")))
	Templates["5xx"] = template.Must(template.ParseFiles(filepath.Join(CurDir, "views/5xx.html")))
	Templates["login"] = template.Must(template.ParseFiles(filepath.Join(CurDir, "views/login.html")))
	Templates["msg_to_login"] = template.Must(template.ParseFiles(filepath.Join(CurDir, "views/msg_to_login.html")))
	Templates["msg_to_homepage"] = template.Must(template.ParseFiles(filepath.Join(CurDir, "views/msg_to_home.html")))
}

//func Render(w http.ResponseWriter, name string, template string, viewModel interface{}) {
//
//	// ensure the template exists int the map
//	tmpl, ok := Templates[name]
//
//	if !ok {
//		http.Error(w, "the template does not exist.", http.StatusInternalServerError)
//		return
//	}
//	err := tmpl.ExecuteTemplate(w, template, viewModel)
//	if err != nil {
//		http.Error(w, err.Error(), http.StatusInternalServerError)
//	}
//}

func NotFound(w http.ResponseWriter, r *http.Request) {

	Templates["404"].Execute(w, nil)
}
