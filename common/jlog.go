package common

import (
	"log"
)

var (
	Info        *log.Logger // 重要信息
	Warn        *log.Logger // 需要关心的事
	Error       *log.Logger // 关键错误信息
	fileName                = "j.log"
	maxBytes    uint        = 1024 * 1024 * 100 // 100M
	backUpCount uint8       = 2
)

func InitLog() {
	c := AppConfig.LogConfig
	if len(c.FileName) > 0 {
		fileName = c.FileName
	}
	if c.MaxBytes > 0 {
		maxBytes = c.MaxBytes
	}
	if c.BackUpCount > 0 {
		backUpCount = c.BackUpCount
	}
	h, err := NewRotatingFileHandler(fileName, maxBytes, backUpCount)
	if err != nil {
		log.Fatal(err)
	}
	Info = log.New(h, "Info: ", log.LstdFlags|log.Lshortfile)
	Warn = log.New(h, "Warn: ", log.LstdFlags|log.Lshortfile)
	Error = log.New(h, "ERR: ", log.LstdFlags|log.Lshortfile)
}
