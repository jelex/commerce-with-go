package common

const PAGE_SIZE = 10

/**
 *	分页数据模型
 */
type Page struct {
	List         interface{}
	PageNo       int
	PageSize     int
	TotalPages   int
	TotalRecords int
	HasPrev      bool
	HasNext      bool
	// 分页使用
	Url string
}

// 或者用泛形
func NewPage(list interface{}, pageNo, pageSize, totalRecords int) *Page {

	page := Page{List: list, PageNo: pageNo, PageSize: pageSize, TotalRecords: totalRecords}

	page.TotalPages = (totalRecords-1)/pageSize + 1
	page.HasNext = pageNo < page.TotalPages
	page.HasPrev = pageNo > 1

	return &page
}

func (p *Page) GetPrevPage() int {
	if p.HasPrev {
		return p.PageNo - 1
	}
	return 1
}

func (p *Page) GetNextPage() int {
	if p.HasNext {
		return p.PageNo + 1
	}
	return p.TotalPages
}
