package common

const (
	ORDER_STATE_CREATED = iota
	ORDER_STATE_PAID
	ORDER_STATE_DELIVERING
	ORDER_STATE_RECEIVED
	ORDER_STATE_FINISHED
	ORDER_STATE_CANCELLED
	ORDER_STATE_REFUND
	ORDER_STATE_RETURN
	ORDER_STATE_DEL
)

const (
	COOKIE_NAME = "SS"
	// CATALOG_SEARCH_USER_KEY 用户分类搜索数据保存key
	CATALOG_SEARCH_USER_KEY = "CS:"

	CatalogJSONLock = "cat_lock"
	CatalogJSON     = "cat:list" // 分类首页数据缓存
	CatalogJSONTTL  = 3600       // 缓存一小时失效

	ReleaseLockLuaScript = `if redis.call("get",KEYS[1]) == ARGV[1]
then
    return redis.call("del",KEYS[1])
else
    return 0
end`

	HOT_KW_LIST  = "hot:kw" // 热点搜索关键词
	HOT_KW_LIMIT = 5        // 热点搜索关键词限制数量

	ORDER_TOKEN_PREFIX = "order:token:user:" // 订单提交防重令牌

	WAIT_PAY_TIME      = 15         // 单位：分钟， 等待会员付款时间
	RELEASE_STOCK_TIME = 20         // 单位：分钟， 用户不付款，不主动取消订单，咱们等待解锁库存时间
	CaptchaPrefix      = "captcha:" // 登录验证码 后台验证前缀
	CaptchaCookie      = "cc"       // 登录后台验证cookie name
)

const (
	FEEDBACK_GOODS = iota + 1
	FEEDBACK_DELIVERY
	FEEDBACK_CUSTOM_SERVICE
	FEEDBACK_PROMOTION
	FEEDBACK_FUNCTION_EX
	FEEDBACK_PRODUCT
	FEEDBACK_OTHER
)

const SEND_EMAIL_CODE_PREFIX = "email:send:"

var fD = make(map[int8]string, 8)

func init() {

	fD[FEEDBACK_GOODS] = "商品相关"
	fD[FEEDBACK_DELIVERY] = "物流状况"
	fD[FEEDBACK_CUSTOM_SERVICE] = "商品相关"
	fD[FEEDBACK_GOODS] = "客户服务"
	fD[FEEDBACK_PROMOTION] = "优惠活动"
	fD[FEEDBACK_FUNCTION_EX] = "功能异常"
	fD[FEEDBACK_PRODUCT] = "产品建议"
	fD[FEEDBACK_OTHER] = "其它"
}

func GetFd() map[int8]string {
	return fD
}
