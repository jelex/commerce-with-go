package req

import "commerce/model"

type OrderDTO struct {
	model.Order
	OrderItems []model.OrderItem
	PayPrice   float64
	Fare       float64
}
