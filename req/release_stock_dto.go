package req

import "commerce/model"

type OrderReleaseStockDto struct {
	TaskId          int
	OrderTaskDetail model.WareOrderTaskDetail
}
