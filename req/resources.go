package req

import (
	"mime/multipart"
)

type (

	// -----------------goods detail img s----------------------

	GoodsImgReq struct {
		F  multipart.File
		Fh *multipart.FileHeader
	}

	AuthReq struct {
		Code     string      `json:"code"`
		UserInfo UserInfoReq `json:"userInfo"`
	}
	UserInfoReq struct {
		//EncryptedData string `json:"encryptedData"`
		//ErrMsg string `json:"errMsg"`
		//Iv string `json:"iv"`
		UserInfo UserInfo `json:"userInfo"`
		//Signature string `json:"signature"`
	}
	UserInfo struct {
		AvatarUrl string `json:"avatarUrl"`
		City      string `json:"city"`
		Country   string `json:"country"`
		Gender    int8   `json:"gender"`
		NickName  string `json:"nickName"`
		Province  string `json:"province"`
	}

	CollectReq struct {
		// 0-没有收藏（对应收藏操作），1-有收藏（对应取消收藏操作）
		HasCollect int8 `json:"hasCollect"`
		// sku id
		ValueId int `json:"valueId"`
	}

	AddToCartReq struct {
		// 商品详情对应的skuId(用户由这个sku 商品详情进来)
		PageSkuId int `json:"pageSkuId"`
		// 购买数量
		Number int `json:"number"`
		// 用户购买的sku (可能和PageSkuId不一样，比如此商品对应的spu有多个sku,用户切换其它sku购买了)
		SkuId int `json:"skuId"`
	}

	CartCheckReq struct {
		SkuId     int    `json:"skuId"`
		IsChecked int8   `json:"isChecked"`
		SkuIds    string `json:"skuIds"` // 全选/取消 全选操作
	}

	CartItemsDeleteReq struct {
		SkuIds string `json:"skuIds"`
	}

	SaveAddrReq struct {
		Id         int    `json:"id"`
		Name       string `json:"userName"`
		TelNumber  string `json:"telNumber"`
		ProvinceId int    `json:"province_id"`
		CityId     int    `json:"city_id"`
		RegionId   int    `json:"district_id"`
		IsDefault  bool   `json:"is_default"`
		Province   string `json:"provinceName"`
		City       string `json:"cityName"`
		Region     string `json:"countyName"`
		DetailInfo string `json:"detailInfo"` // 详细地址
	}

	SubmitOrderReq struct {
		AddressId  int     `json:"addressId"`  // 收货地址id
		CouponId   int     `json:"couponId"`   // 卡券id
		OrderToken string  `json:"orderToken"` // 防重令牌：防止重复提交
		PayPrice   float64 `json:"payPrice"`   // 应付金额：用于验价
		Type       int8    `json:"type"`       // 0-直接购买，1-购物车

		MemberId int // 辅助字段，非前端传递
	}

	DeleteAddrReq struct {
		Id int `json:"id"`
	}

	AddFeedbackReq struct {
		Content string `json:"content"`
		Mobile  string `json:"mobile"`
		Index   int8   `json:"index"`
	}

	EmailCodeReq struct {
		Email string `json:"email"`
	}

	BindEmailReq struct {
		EmailCode string `json:"emailCode"`
		Email     string `json:"email"`
	}

	PostCommentReq struct {
		OrderId int    `json:"orderId"`
		SkuId   int    `json:"skuId"`
		Content string `json:"content"`

		MemberIp       string
		MemberId       int
		MemberNickName string
		MemberAvatar   string
	}
)
