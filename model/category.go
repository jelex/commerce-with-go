package model

import "time"

// 商品类别

type Category struct {
	Id             int
	Parent         int    // 父类别
	Name           string // 名称
	MainImg        string //主图
	Description    string // 描述说明
	Status         int8   // 状态
	Priority       int    // 优先级，值越大，排名越靠前
	CreatedTime    time.Time
	CreatedTimeStr string
	UpdatedTime    time.Time

	// vo字段
	ParentCategories []Category
}
