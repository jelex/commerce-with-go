package model

import "time"

type Order struct {
	Id          int
	OrderNo     string
	CreateTime  time.Time
	UpdatedTime time.Time
	TotalCount  int
	TotalAmount float64 // 应付
	State       int8    // 订单状态,constant.go 看常量
	UserId      int     // 下单人用户id

	PayAmount      float64 // 实付
	DiscountAmount float64 // 优惠金额
	FreightAmount  float64 // 运费

	CouponAmount float64 // 卡券抵扣的金额

	Integration       int     // 使用的积分数量
	IntegrationAmount float64 // 积分抵扣金额

	GiftIntegration int // 赠送积分数量
	GiftGrowth      int // 赠送成长值

	AutoConfirmDay int8 // 自动确认天数

	ReceiverUserName   string // 收货人姓名
	ReceiverPhone      string // 收货人手机号
	ReceiverPostCode   string // 收货人邮编
	ReceiverProvince   string // 收货人所在省份
	ReceiverCity       string // 收货人所在市
	ReceiverRegion     string // 收货人所在区、镇、乡
	ReceiverDetailAddr string // 收货人详细地址

	IsValid int8 // 是否有效，1-是，0-否，无效订单不在applet端展示，只在管理员后台可见

	// vo使用
	CreatedTimeStr string
	UpdatedTimeStr string
	Addr           string
	StateStr       string
	OrderItemList  []OrderItem
}

type OrderStatus struct {
	Id          int
	OrderId     int
	Status      int8
	CreatedTime time.Time

	StatusStr      string
	CreatedTimeStr string
}
