package model

/**
  帮助中心：问答相关表
*/

type HelpIssue struct {
	Id       int
	TypeName string
	Sort     int8
	IsValid  int8 // 是否有效，1-是，0-否
}

type HelpInfo struct {
	Id       int
	TypeId   int
	Question string
	Answer   string
	Sort     int8
	IsValid  int8 // 是否有效，1-是，0-否

	// vo 字段
	TypeName    string
	AllTypeList []HelpIssue
}
