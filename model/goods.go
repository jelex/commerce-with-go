package model

import "time"

/** spu */

type Goods struct {
	Id          int
	Name        string
	Description string
	Priority    int  // 优先级，值越大，越靠前
	Status      int8 // 1-上架，0-下架
	CreatedTime time.Time
	UpdatedTime time.Time
	MainImg     string // 主图

	BrandId    int // 品牌
	CategoryId int // 类别，设计简单点，一个商品只属于一个类别
	// vo 字段
	CreatedTimeStr     string
	CategoryName       string
	Categories         []Category
	GoodsAttrValues    []GoodsAttrValue // 回显
	AllGoodsAttrValues []GoodsAttr      // 所有
	GoodsDetailImgList []GoodsImg

	BrandList []Brand

	BrandName string
	Logo      string
	BrandDesc string

	CMainImg string
	CDesc    string
}
