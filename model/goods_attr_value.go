package model

// spu 规格属性

type GoodsAttrValue struct {
	Id       int
	GoodsId  int
	AttrId   int    // 属性id, GoodsAttr表的主键
	AttrName string // 属性名称 GoodsAttr表的名称
}
