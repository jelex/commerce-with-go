package model

// 会员收获地址 数据模型

type MemberReceiveAddr struct {
	Id            int
	MemberId      int
	Name          string
	Phone         string
	PostCode      string
	ProvinceId    int
	Province      string
	CityId        int
	City          string
	RegionId      int
	Region        string
	DetailAddr    string
	AreaCode      string //省市区代码
	DefaultStatus int8   // 是否默认地址 1-是，0-否
}
