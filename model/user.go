package model

import "time"

type User struct {
	Id          int
	Username    string
	Password    string
	Email       string
	CreatedTime time.Time
}
