package model

type CategoryBrandRel struct {
	Id           int
	BrandId      int
	CategoryId   int
	BrandName    string
	CategoryName string
}
