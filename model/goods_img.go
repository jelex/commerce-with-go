package model

// 商品详情图

type GoodsImg struct {
	Id      int
	GoodsId int    // 商品id
	ImgUrl  string // 图片url
	Sort    int8   // 排序值
}
