package model

// sku 销售属性

type SkuAttrValue struct {
	Id        int
	SkuId     int
	AttrId    int    // 属性id, GoodsAttr表的主键
	AttrName  string // 属性名称 GoodsAttr表的名称
	AttrValue string // 商品的 【一个】属性值

	// vo
	SkuIds string
}
