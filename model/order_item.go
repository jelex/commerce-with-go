package model

import "time"

type OrderItem struct {
	Id     int
	Count  int     // 数量
	Amount float64 // 小计

	SkuPrice float64 // 单价
	SkuId    int     // sku id
	SkuName  string  // sku 名称
	SkuImg   string  // sku 图片

	SpuId   int    // 商品id
	SpuName string // 商品名称

	SkuAttrValue string // sku 销售规格属性

	BrandId    int // 品牌id
	CategoryId int // 商品类别id

	OrderId int    // 订单id
	OrderNo string // 订单号

	CouponAmount float64 // 卡券抵扣的金额

	Integration       int     // 使用的积分数量
	IntegrationAmount float64 // 积分抵扣金额

	GiftIntegration int // 赠送积分数量
	GiftGrowth      int // 赠送成长值

	DiscountAmount float64 // 总优惠金额（优惠券+积分抵扣 ... 之和）

	RealAmount float64 // 实际需要支付金额

	Commented   int8 // 是否已评价，0-否，1-是
	UpdatedTime time.Time
}
