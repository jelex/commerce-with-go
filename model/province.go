package model

type Province struct {
	Id         int
	ProvinceId int
	Province   string
}

type City struct {
	Id     int
	CityId int
	City   string

	ProvinceId int
}

// 区（region）

type Area struct {
	Id     int
	AreaId int
	Area   string

	CityId int
}
