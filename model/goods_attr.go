package model

// 商品属性：颜色，尺寸，通用 等属性

type GoodsAttr struct {
	Id         int
	Name       string
	Options    string // 可选值列表
	CategoryId int    // 商品类别
	Enabled    int8   // 是否启用，1-是，0-否

	// vo
	CategoryName string
	Categories   []Category
}
