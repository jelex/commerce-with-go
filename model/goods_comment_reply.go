package model

//  商品评价回复关系

type GoodsCommentReply struct {
	Id        int
	CommentId int
	ReplyId   int
}
