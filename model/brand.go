package model

// 品牌

type Brand struct {
	Id          int
	Name        string
	Logo        string
	Description string
	ShowStatus  int8
	FirstLetter string
	Sort        int

	// vo
	CategoryId    int
	CategoryName  string
	Categories    []Category
	OwnCategories []Category
}
