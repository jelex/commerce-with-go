package model

import "time"

// 商品sku 收藏表 数据模型

type GoodsCollect struct {
	Id          int
	SkuId       int
	MemberId    int
	CreatedTime time.Time
}
