package model

import "time"

type Feedback struct {
	Id          int
	MemberId    int
	MemberName  string
	MemberIp    string
	Phone       string
	Content     string
	Type        int8 // 看 constangt.go#FEEDBACK
	CreatedTime time.Time

	// vo
	TypeStr        string
	CreatedTimeStr string
}
