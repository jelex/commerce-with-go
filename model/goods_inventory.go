package model

import "time"

type GoodsInventory struct {
	Id            int
	SkuId         int
	Sales         int       // 销售量
	BoughtUserNum int       // 购买的人数
	Stock         int       // 库存
	Locked        int       // 锁定的库存数量
	Viewed        int       // 看过的人数
	UpdatedTime   time.Time // 更新时间

	UpdatedTimeStr string
	AllSkus        []Sku
	RealPrice      float64
	MainImg        string // sku 主图
}

type WareOrderTask struct {
	Id          int
	OrderId     int
	OrderNo     string
	TaskStatus  int8 // 1-进行中，2-已结束
	CreatedTime time.Time
	WareId      int // 暂时不用
}

type WareOrderTaskDetail struct {
	Id         int
	SkuId      int
	SkuName    string
	SkuNum     int
	TaskId     int
	WareId     int  // 暂时不用
	LockStatus int8 // 1-锁定，2-解锁
}
