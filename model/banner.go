package model

import "time"

// 首页banner数据模型

type Banner struct {
	Id          int
	Link        string
	ImgUrl      string
	Priority    int8
	IsValid     int8
	CreatedTime time.Time
	UpdatedTime time.Time

	CreatedTimeStr string
}
