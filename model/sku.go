package model

import "time"

/** sku*/

type Sku struct {
	Id          int
	Name        string
	GoodsId     int
	Description string
	Price       float64 // 原价
	RealPrice   float64 // 真实价格：交易价格
	Priority    int     // 优先级，值越大，越靠前
	Status      int8    // 1-有效，2-无效
	CreatedTime time.Time
	UpdatedTime time.Time
	MainImg     string // 主图
	IsHot       int8   // 是否热卖单品 1-是，0-否

	// vo 字段
	CreatedTimeStr    string
	GoodsName         string
	AllGoods          []Goods
	SkuAttrValues     []SkuAttrValueVo
	OwnSaleAttrValues []SkuAttrValue
	BrandId           int
	CategoryId        int

	GoodsImg  string
	GoodsDesc string

	BrandName    string
	Logo         string
	BrandDesc    string
	CategoryName string
	CMainImg     string
	CDesc        string
}

type SkuAttrValueVo struct {
	AttrId   int    // 属性id, GoodsAttr表的主键
	AttrName string // 属性名称 GoodsAttr表的名称

	Options        string // 辅助
	AttrOptionList []SkuOptionKV
}

type SkuOptionKV struct {
	// 由attrId_option构成
	OptionVal string
	// 由attrName_option构成
	OptionName string
}
