package model

import "time"

// app 会员

type Member struct {
	Id       int
	Phone    string
	Name     string
	NickName string
	Avatar   string // 头像
	Birth    time.Time
	Gender   int8 // 1-男，2-女，0-未知

	OpenId     string
	SessionKey string

	Country  string
	Province string
	City     string

	Status      int8 // 用户状态
	CreatedTime time.Time
	UpdatedTime time.Time

	CreatedTimeStr string
	UpdatedTimeStr string
	BirthStr       interface{}
	GenderStr      string
}
