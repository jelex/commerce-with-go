package model

import "time"

type GoodsComment struct {
	Id             int
	SkuId          int
	GoodsId        int
	GoodsName      string
	MemberNickName string
	Star           int8
	MemberIp       string
	ShowStatus     int8
	SkuSpecs       string
	LikeCount      int
	ReplyCount     int
	Resources      string // 评论图片/视频[json数据；[{type:文件类型,url:资源路径}]]
	Content        string
	MemberAvatar   string // 会员头像
	CommentType    int8   // 评论类型[0 - 对商品的直接评论，1 - 对评论的回复]

	CreatedTime    time.Time
	CreatedTimeStr string
}
