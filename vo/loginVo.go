package vo

type LoginVo struct {
	Username string
	Password string
	Captcha  string
	Msg      string
}
