package vo

import "commerce/model"

type MyOrderVo struct {
	Id         int     `json:"id"`
	OrderNo    string  `json:"order_sn"`
	OrderState string  `json:"order_state"`
	PayAmount  float64 `json:"actual_price"`
	UnPay      bool    `json:"unPay"`
	Comment    bool    `json:"comment"`

	GoodsList []OrderGoodsVo `json:"goodsList"`
}

type OrderDetailPageVo struct {
	Order      model.Order
	OrderItems []model.OrderItem

	SkuList []model.Sku

	Member model.Member

	OrderStatusList []model.OrderStatus
}

type OrderGoodsVo struct {
	Id             int     `json:"id"`
	SkuId          int     `json:"skuId"`
	SkuImg         string  `json:"sku_img"`
	SkuName        string  `json:"sku_name"`
	Count          int     `json:"count"`
	SpecAttrValues string  `json:"spec_attr_values"`
	RetailPrice    float64 `json:"retail_price"`
	CanComment     bool    `json:"can_comment"`
}

type PayOptionVo struct {
	Pay     bool `json:"pay"`
	Confirm bool `json:"confirm"`
	Comment bool `json:"comment"`
}
type OrderDetail struct {
	Id            int     `json:"id"`
	OrderNo       string  `json:"order_sn"`
	OrderState    int8    `json:"order_state"`
	OrderStateStr string  `json:"order_state_str"`
	PayAmount     float64 `json:"actual_price"`
	CreatedTime   string  `json:"created_time"`
	Consignee     string  `json:"consignee"` // 收件人
	Mobile        string  `json:"mobile"`
	FullRegion    string  `json:"full_region"`
	Address       string  `json:"address"`

	TotalAmount   float64 `json:"total_amount"`
	FreightAmount float64 `json:"freight_amount"`
}

type OrderDetailVo struct {
	OrderInfo OrderDetail `json:"orderInfo"`

	OrderGoods []OrderGoodsVo `json:"orderGoodsList"`

	HandleOption PayOptionVo `json:"handleOption"`
}
