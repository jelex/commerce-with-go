package vo

// 后台首页resp 数据模型

type IndexVo struct {
	Username    string
	NewOrderNum int
}

// 新品首发

type NewGoodsVo struct {
	NewGoodsList []AppSku `json:"newGoodsList"`
}

type IndexCategoryVo struct {
	CategoryList []SingleCategoryVo `json:"categoryList"`
}
type SingleCategoryVo struct {
	Id        int      `json:"id"`
	Name      string   `json:"name"`
	GoodsList []AppSku `json:"goodsList"`
}

type IndexHotGoodsVo struct {
	HotGoodsList []AppSku `json:"hotGoodsList"`
}

type IndexBannerVo struct {
	BannerList []BannerVo `json:"banner"`
}
type BannerVo struct {
	Id     int    `json:"id"`
	Link   string `json:"link"`
	ImgUrl string `json:"image_url"`
}

type IndexBrandVo struct {
	BrandList []BrandVo `json:"brandList"`
}
type BrandVo struct {
	Id          int    `json:"id"`
	Name        string `json:"name"`
	ImgUrl      string `json:"new_pic_url"`
	Description string `json:"description"`
	//FloorPrice float64 `json:"floor_price"`
}

type BrandDetailVo struct {
	Brand BrandVo `json:"brand"`
}
