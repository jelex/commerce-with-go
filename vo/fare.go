package vo

import "commerce/model"

type FareVo struct {
	Addr         model.MemberReceiveAddr // 收货人地址信息
	FreightPrice float64                 // 运费
}
