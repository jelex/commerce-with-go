package vo

import "net/http"

type (
	R struct {
		ErrNo int         `json:"errno"`
		Msg   string      `json:"errMsg"`
		Data  interface{} `json:"data"`
	}

	IndexChannelVo struct {
		ChannelList []Channel `json:"channel"`
	}

	Channel struct {
		IconUrl string `json:"icon_url"`
		Name    string `json:"name"`
	}

	AuthVo struct {
		UserInfo MemberVo `json:"userInfo"`
		Token    string   `json:"token"`
		UserId   int      `json:"userId"`
	}
	MemberVo struct {
		UserId   int    `json:"userId"`
		Nickname string `json:"nickname"`
		Avatar   string `json:"avatar"`
	}
)

func Err(msg string) R {
	return New(-1, msg, nil)
}
func OrderTokenErr(msg string) R {
	return New(402, msg, nil)
}

func Ok(data interface{}) R {
	return New(0, "", data)
}

func Ok2(data interface{}) R {
	return New(401, "", data)
}

func New(errno int, msg string, data interface{}) R {
	return R{errno, msg, data}
}

func TokenErr() R {
	return R{http.StatusUnauthorized, "", nil}
}
