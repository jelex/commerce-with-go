package vo

type Cart struct {

	//count int
	//amount float64
	CartItems []CartItem `json:"cartItems"`

	//UserId int

	CartTotal CartTotal `json:"cartTotal"`
}

type CartTotal struct {
	CheckedGoodsCount  int     `json:"checkedGoodsCount"`
	CheckedGoodsAmount float64 `json:"checkedGoodsAmount"`
}

// 价格：总计
func (c *Cart) GetAmount() float64 {

	var amount float64
	for _, item := range c.CartItems {
		amount += item.GetAmount()
	}
	return amount
}

// 数量：总计
func (c *Cart) GetCount() int {

	var count int
	for _, item := range c.CartItems {
		count += item.Count
	}
	return count
}
