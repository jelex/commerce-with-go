package vo

type AppGoodsVo struct {
	GoodsList          []AppSku      `json:"goodsList"`
	CurrentPage        int           `json:"currentPage"`
	TotalPages         int           `json:"totalPages"`
	PageSize           int           `json:"pageSize"`
	FilterCategoryList []CategoryNav `json:"filterCategoryList"`
}

type AppSku struct {
	Id          int     `json:"id"`
	MainImg     string  `json:"list_pic_url"`
	Name        string  `json:"name"`
	RetailPrice float64 `json:"retail_price"`
	Desc        string  `json:"goods_brief"`
}

type GoodsCountVo struct {
	GoodsCount int `json:"goodsCount"`
}

// 商品详情页面数据模型

type GoodsSkuDetailVo struct {
	Sku               AppSku            `json:"info"`              // sku 基本信息
	GalleryList       []GoodsImgVo      `json:"gallery"`           // 商品详情图列表
	Attribute         []AttributeVo     `json:"attribute"`         // 当前sku商品销售规格属性
	IssueList         []IssueVo         `json:"issueList"`         // 问答
	Comment           CommentVo         `json:"comment"`           // 评价列表
	Brand             BrandVo           `json:"brand"`             // 品牌
	SpecificationList []SpecificationVo `json:"specificationList"` // 销售规格
	ProductList       []SkuStockVo      `json:"productList"`       // sku-id及其库存
	UserHasCollect    int               `json:"userHasCollect"`    // 是否有收藏
}

// 商品详情图

type GoodsImgVo struct {
	Id     int    `json:"id"`
	ImgUrl string `json:"img_url"`
}

type AttributeVo struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

type IssueVo struct {
	Id       int    `json:"id"`
	Question string `json:"question"`
	Answer   string `json:"answer"`
}

type CommentVo struct {
	Count int             `json:"count"`
	Data  CommentDetailVo `json:"data"`
}
type CommentDetailVo struct {
	Id       int                  `json:"id"`
	Avatar   string               `json:"avatar"`
	Nickname string               `json:"nickname"`
	AddTime  string               `json:"add_time"`
	Content  string               `json:"content"`
	ImgList  []CommentDetailImgVo `json:"pic_list"`
}
type CommentDetailImgVo struct {
	Id     int    `json:"id"`
	ImgUrl string `json:"pic_url"`
}

type SpecificationVo struct {
	AttrId    int             `json:"specification_id"`
	Name      string          `json:"name"`
	ValueList []Specification `json:"valueList"`
}
type Specification struct {
	Id      string `json:"id"`
	Value   string `json:"value"`
	AttrId  int    `json:"specification_id"`
	Checked bool   `json:"checked"`
	SkuIds  string `json:"skuIds"`
}

type SkuStockVo struct {
	SkuId     int     `json:"skuId"`
	Stock     int     `json:"stock"`
	RealPrice float64 `json:"retail_price"`
	MainImg   string  `json:"mainImg"`
}

type GoodsCommentCountVo struct {
	AllCount    int `json:"allCount"`
	HasPicCount int `json:"hasPicCount"`
}

type GoodsCommentListVo struct {
	List        []CommentDetailVo `json:"list"`
	CurrentPage int               `json:"currentPage"`
}
