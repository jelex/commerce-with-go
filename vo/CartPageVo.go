package vo

type CartPageVo struct {
	*Cart
	UserId   int
	Username string
}

type CartGoodsVo struct {
	CartTotal CartTotalVo `json:"cartTotal"`
}
type CartTotalVo struct {
	GoodsCount int `json:"goodsCount"`
}

type CheckoutCartVo struct {
	CheckedGoodsList []CartItem    `json:"checkedGoodsList"`
	CheckedAddress   CheckedAddrVo `json:"checkedAddress"`
	ActualPrice      float64       `json:"actualPrice"` // 实际需要支付的总价
	CouponList       []CouponVo    `json:"couponList"`
	CouponPrice      float64       `json:"couponPrice"`     // 优惠券（能抵扣的）金额
	FreightPrice     float64       `json:"freightPrice"`    // 快递费
	GoodsTotalPrice  float64       `json:"goodsTotalPrice"` // 商品总价
	OrderTotalPrice  float64       `json:"orderTotalPrice"` // 订单总价（为了不引起纠纷，和实际支付总价保持一致）
	OrderToken       string        `json:"orderToken"`      // 防重令牌
}
type CheckedAddrVo struct {
	Id         int    `json:"id"`
	Name       string `json:"userName"`
	IsDefault  int8   `json:"is_default"`
	TelNumber  string `json:"telNumber"`
	FullRegion string `json:"full_region"` // 省市区
	DetailInfo string `json:"detailInfo"`  // 详细地址

	ProvinceId   int    `json:"province_id"`
	ProvinceName string `json:"province_name"`
	CityId       int    `json:"city_id"`
	CityName     string `json:"city_name"`
	RegionId     int    `json:"district_id"`
	RegionName   string `json:"district_name"`
}
type CouponVo struct {
	// @ todo
}
