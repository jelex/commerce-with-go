package vo

type HelpTypeVo struct {
	Id       int    `json:"id"`
	TypeName string `json:"typeName"`
}

type HelpInfoVo struct {
	Id       int    `json:"id"`
	Question string `json:"question"`
	Answer   string `json:"answer"`
}
