package vo

// app 分类搜索 search index 数据模型

type CatalogIndexVo struct {

	// 只用到 id + name 2个字段
	CategoryList    []CategoryVo `json:"categoryList"`
	CurrentCategory CategoryVo   `json:"currentCategory"`
}

type CategoryVo struct {
	Id   int    `json:"id"`
	Name string `json:"name"`

	WapBannerUrl string `json:"wap_banner_url"`
	FrontName    string `json:"front_name"`

	// 只用到 id + Name + WapBannerUrl 3个字段
	SubCategoryList []CategoryVo `json:"subCategoryList"`
}

type SearchIndexVo struct {
	// 保存在前端本地
	//HistoryKeyword []string `json:"historyKeywordList"`
	DefaultKeyword string `json:"defaultKeyword"`
	HotKeyword     []Hk   `json:"hotKeywordList"`
}

type Hk struct {
	IsHot   int    `json:"is_hot"`
	Keyword string `json:"keyword"`
}

type GoodsCategoryVo struct {
	NavList         []CategoryNav `json:"brotherCategory"`
	CurrentCategory CategoryNav   `json:"currentCategory"`
}

type CategoryNav struct {
	Id        int    `json:"id"`
	Name      string `json:"name"`
	FrontName string `json:"front_name"`
	//MainImg string `json:"wap_banner_url"`
}
