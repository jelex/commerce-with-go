package vo

type CartItem struct {
	SkuId   int     `json:"skuId"`
	Checked bool    `json:"checked"`
	Price   float64 `json:"price"`
	Count   int     `json:"count"`

	SkuName string `json:"skuName"`
	SkuImg  string `json:"skuImg"`

	SkuAttrs []string

	SpecNameAttrs string `json:"spec_name_attrs"`

	// 辅助排序字段
	Sort int
}

// 计算小计

func (cartItem *CartItem) GetAmount() float64 {

	return cartItem.Price * float64(cartItem.Count)
}
