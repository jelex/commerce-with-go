package vo

type RegionVo struct {
	Id       int    `json:"id"`
	Name     string `json:"name"`
	Type     int8   `json:"type"`
	ParentId int    `json:"parent_id"`
}
