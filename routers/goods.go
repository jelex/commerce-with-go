package routers

import (
	"commerce/common"
	"commerce/controllers"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func SetGoodsRoutes(router *mux.Router) *mux.Router {

	indexRouter := mux.NewRouter()

	indexRouter.HandleFunc("/commerce/api/v2/goods/list", controllers.ListGoods).Methods("GET")
	indexRouter.HandleFunc("/commerce/api/v2/goods/addUI", controllers.AddGoodsUIHandler).Methods("GET")
	indexRouter.HandleFunc("/commerce/api/v2/goods/updateUI", controllers.GetGoodsHandler).Methods("GET")
	indexRouter.HandleFunc("/commerce/api/v2/goods/addOrEdit", controllers.AddOrEditGoodsHandler).Methods("POST")
	indexRouter.HandleFunc("/commerce/api/v2/goods/onSale", controllers.OnSaleGoodsHandler).Methods("POST")
	indexRouter.HandleFunc("/commerce/api/v2/goods/offSale", controllers.OffSaleGoodsHandler).Methods("POST")

	router.PathPrefix("/commerce/api/v2/goods").Handler(negroni.New(
		negroni.HandlerFunc(common.Authorize),
		negroni.Wrap(indexRouter),
	))
	return router
}
