package routers

import (
	"commerce/common"
	"commerce/controllers"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func SetGoodsInventoryRoutes(router *mux.Router) *mux.Router {

	goodsInventoryRouter := mux.NewRouter()

	subrouter := goodsInventoryRouter.PathPrefix("/commerce/api/v2/inventory").Subrouter()

	subrouter.HandleFunc("/skuId", controllers.GetStockBySkuId).Methods("GET")
	subrouter.HandleFunc("/list", controllers.ListGoodsInventory).Methods("GET")
	subrouter.HandleFunc("/addUI", controllers.AddGoodsInventoryUIHandler).Methods("GET")
	subrouter.HandleFunc("/updateUI", controllers.GetGoodsInventoryHandler).Methods("GET")
	subrouter.HandleFunc("/addOrEdit", controllers.AddOrEditGoodsInventoryHandler).Methods("POST")

	router.PathPrefix("/commerce/api/v2/inventory").Handler(negroni.New(
		negroni.HandlerFunc(common.Authorize),
		negroni.Wrap(goodsInventoryRouter),
	))
	return router
}
