package routers

import (
	"commerce/common"
	"commerce/routers/applet"
	"github.com/gorilla/mux"
	"net/http"
)

// 在main.go的init()中被调用

func InitRoutes() *mux.Router {

	router := mux.NewRouter().StrictSlash(false)

	// 静态文件映射
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/",
		http.FileServer(http.Dir("views/static/"))))

	router.PathPrefix("/views/imgs/").Handler(http.StripPrefix("/views/imgs/",
		http.FileServer(http.Dir("views/imgs/"))))

	// applet 接口模块开始----------------------

	// 首页
	router = applet.SetIndexRoutes(router)
	// 分类
	router = applet.SetCatalogRoutes(router)
	// 商品
	router = applet.SetAppGoodsRoutes(router)
	// 商品(sku) 收藏
	router = applet.SetGoodsCollectRoutes(router)
	// 品牌
	router = applet.SetAppBrandRoutes(router)
	// 购物车
	router = applet.SetCartRouters(router)
	// 收获地址
	router = applet.SetAddrRouters(router)
	// 订单&支付
	router = applet.SetOrderPayRouters(router)
	// 帮助中心
	router = applet.SetHelpRoutes(router)
	// 意见反馈
	router = applet.SetFeedbackRoutes(router)
	// 用户模块
	router = applet.SetUserRoutes(router)
	// 评论模块
	router = applet.SetCommentRoutes(router)

	// -----------------applet接口结束-------------------------

	// -----------------后台接口开始-------------------------
	// 首页
	router = SetIndexRoutes(router)
	// banner
	router = SetBannerRoutes(router)
	// 分类
	router = SetCategoryRoutes(router)
	// 品牌
	router = SetBrandRoutes(router)
	// 商品属性
	router = SetGoodsAttrRoutes(router)
	// 商品
	router = SetGoodsRoutes(router)
	// 商品sku
	router = SetSkuRoutes(router)
	// 库存 & 销量
	router = SetGoodsInventoryRoutes(router)
	// 订单
	router = SetOrderRoute(router)

	// oss get policy
	router = SetOssRoute(router)

	// 帮助中心
	router = SetHelpRoute(router)

	// 意见反馈
	router = SetAdminFeedbackRoute(router)

	// 会员用户
	router = SetUserRoute(router)
	// 会员商品评价
	router = SetCommentRoute(router)
	// 后台用户 (要放在最后)
	router = SetAdminRoute(router)

	// -----------------后台接口结束-------------------------

	// 404 处理
	//router.NotFoundHandler = router.NewRoute().HandlerFunc(common.NotFound).GetHandler()
	router.NotFoundHandler = http.HandlerFunc(common.NotFound)

	return router
}
