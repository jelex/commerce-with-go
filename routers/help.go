package routers

import (
	"commerce/common"
	"commerce/controllers"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func SetHelpRoute(router *mux.Router) *mux.Router {

	orderRouter := mux.NewRouter()
	subrouter := orderRouter.PathPrefix("/commerce/api/v2/help").Subrouter()

	subrouter.HandleFunc("/list", controllers.ListHelpIssueHandler).Methods("GET")
	subrouter.HandleFunc("/addUI", controllers.AddHelpIssueUIHandler).Methods("GET")
	subrouter.HandleFunc("/updateUI", controllers.GetHelpIssueHandler).Methods("GET")
	subrouter.HandleFunc("/addOrEdit", controllers.AddOrEditHelpIssueHandler).Methods("POST")
	subrouter.HandleFunc("/updateIssueStatus", controllers.UpdateHelpIssueStatusHandler).Methods("POST")

	subrouter.HandleFunc("/qaList", controllers.ListHelpInfoHandler).Methods("GET")
	subrouter.HandleFunc("/qaAddUI", controllers.AddHelpInfoUIHandler).Methods("GET")
	subrouter.HandleFunc("/qaUpdateUI", controllers.GetHelpInfoHandler).Methods("GET")
	subrouter.HandleFunc("/qaAddOrEdit", controllers.AddOrEditHelpInfoHandler).Methods("POST")
	subrouter.HandleFunc("/updateInfoStatus", controllers.UpdateHelpInfoStatusHandler).Methods("POST")

	router.PathPrefix("/commerce/api/v2/help").Handler(negroni.New(
		negroni.HandlerFunc(common.Authorize),
		negroni.Wrap(orderRouter),
	))
	return router
}
