package routers

import (
	"commerce/common"
	"commerce/controllers"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func SetGoodsAttrRoutes(router *mux.Router) *mux.Router {

	indexRouter := mux.NewRouter()

	indexRouter.HandleFunc("/commerce/api/v2/goodsAttr/list", controllers.ListGoodsAttr).Methods("GET")
	indexRouter.HandleFunc("/commerce/api/v2/goodsAttr/addUI", controllers.AddGoodsAttrUIHandler).Methods("GET")
	indexRouter.HandleFunc("/commerce/api/v2/goodsAttr/updateUI", controllers.GetGoodsAttrHandler).Methods("GET")
	indexRouter.HandleFunc("/commerce/api/v2/goodsAttr/addOrEdit", controllers.AddOrEditGoodsAttrHandler).Methods("POST")
	indexRouter.HandleFunc("/commerce/api/v2/goodsAttr/delete", controllers.DeleteGoodsAttrHandler).Methods("GET")

	router.PathPrefix("/commerce/api/v2/goodsAttr").Handler(negroni.New(
		negroni.HandlerFunc(common.Authorize),
		negroni.Wrap(indexRouter),
	))
	return router
}
