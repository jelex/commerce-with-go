package routers

import (
	"commerce/common"
	"commerce/controllers"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func SetSkuRoutes(router *mux.Router) *mux.Router {

	indexRouter := mux.NewRouter()

	indexRouter.HandleFunc("/commerce/api/v2/sku/list", controllers.ListSku).Methods("GET")
	indexRouter.HandleFunc("/commerce/api/v2/sku/addUI", controllers.AddSkuUIHandler).Methods("GET")
	indexRouter.HandleFunc("/commerce/api/v2/sku/updateUI", controllers.GetSkuHandler).Methods("GET")
	indexRouter.HandleFunc("/commerce/api/v2/sku/listSkuAttrValue", controllers.ListSkuAttrValueByGoodsId).Methods("GET")
	indexRouter.HandleFunc("/commerce/api/v2/sku/addOrEdit", controllers.AddOrEditSkuHandler).Methods("POST")
	indexRouter.HandleFunc("/commerce/api/v2/sku/onSale", controllers.OnSaleSkuHandler).Methods("POST")
	indexRouter.HandleFunc("/commerce/api/v2/sku/offSale", controllers.OffSaleSkuHandler).Methods("POST")
	indexRouter.HandleFunc("/commerce/api/v2/sku/updateHotStatus", controllers.UpdateHotStatus).Methods("POST")

	router.PathPrefix("/commerce/api/v2/sku").Handler(negroni.New(
		negroni.HandlerFunc(common.Authorize),
		negroni.Wrap(indexRouter),
	))
	return router
}
