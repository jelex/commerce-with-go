package routers

import (
	"commerce/common"
	"commerce/controllers"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

// 后台 管理 会员商品评价

func SetCommentRoute(router *mux.Router) *mux.Router {

	commentRoute := mux.NewRouter()

	subrouter := commentRoute.PathPrefix("/commerce/api/v2/comment").Subrouter()

	subrouter.HandleFunc("/list", controllers.ListComment).Methods("GET")
	subrouter.HandleFunc("/updateStatus", controllers.UpdateCommentStatusHandler).Methods("POST")

	router.PathPrefix("/commerce/api/v2/comment").Handler(negroni.New(
		negroni.HandlerFunc(common.Authorize),
		negroni.Wrap(commentRoute),
	))
	return router
}
