package routers

import (
	"commerce/common"
	"commerce/controllers"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func SetAdminRoute(router *mux.Router) *mux.Router {

	indexRouter := mux.NewRouter()
	indexRouter.HandleFunc("/commerce/api/v2/admin/list", controllers.ListAdmin).Methods("GET")
	indexRouter.HandleFunc("/commerce/api/v2/admin/addUI", controllers.AddAdminUIHandler).Methods("GET")
	indexRouter.HandleFunc("/commerce/api/v2/admin/updateUI", controllers.GetAdminHandler).Methods("GET")
	indexRouter.HandleFunc("/commerce/api/v2/admin/addOrEdit", controllers.AddOrEditAdminHandler).Methods("POST")
	indexRouter.HandleFunc("/commerce/api/v2/admin/delete", controllers.DeleteAdminHandler).Methods("POST")

	router.PathPrefix("/commerce/api/v2/admin").Handler(negroni.New(
		negroni.HandlerFunc(common.Authorize),
		negroni.Wrap(indexRouter),
	))

	loginRouter := mux.NewRouter()

	loginRouter.HandleFunc("/", controllers.RootPageHandler).Methods("GET")
	loginRouter.HandleFunc("/msg_to_login.html", controllers.MsgToLoginPageHandler).Methods("GET")
	loginRouter.HandleFunc("/msg_to_homepage.html", controllers.MsgToHomePageHandler).Methods("GET")
	loginRouter.HandleFunc("/login.html", controllers.LoginPageHandler).Methods("GET")
	loginRouter.HandleFunc("/captcha", controllers.GetCaptcha).Methods("GET") // 获取图片验证码
	loginRouter.HandleFunc("/login", controllers.LoginHandler).Methods("POST")
	loginRouter.HandleFunc("/logout", controllers.LogoutHandler).Methods("POST")
	loginRouter.HandleFunc("/resetPwdToUserEmail", controllers.SendResetPwdToUserEmail).Methods("POST")

	router.PathPrefix("/").Handler(negroni.New(
		negroni.Wrap(loginRouter),
	))

	return router
}
