package routers

import (
	"commerce/common"
	"commerce/controllers"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

// 后台 管理 会员

func SetUserRoute(router *mux.Router) *mux.Router {

	userRoute := mux.NewRouter()

	subrouter := userRoute.PathPrefix("/commerce/api/v2/user").Subrouter()

	subrouter.HandleFunc("/list", controllers.ListUser).Methods("GET")
	subrouter.HandleFunc("/updateStatus", controllers.UpdateUserStatusHandler).Methods("POST")

	router.PathPrefix("/commerce/api/v2/user").Handler(negroni.New(
		negroni.HandlerFunc(common.Authorize),
		negroni.Wrap(userRoute),
	))
	return router
}
