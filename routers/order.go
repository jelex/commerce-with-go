package routers

import (
	"commerce/common"
	"commerce/controllers"
	"commerce/ws"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
	"net/http"
)

func SetOrderRoute(router *mux.Router) *mux.Router {

	hub := ws.H
	go hub.Run()

	orderRouter := mux.NewRouter()
	subrouter := orderRouter.PathPrefix("/commerce/api/v2/order").Subrouter()

	subrouter.HandleFunc("/list", controllers.ListOrderHandler).Methods("GET")
	subrouter.HandleFunc("/detail", controllers.GetOrderDetailHandler).Methods("GET")
	subrouter.HandleFunc("/delete", controllers.DeleteOrderHandler).Methods("POST")
	subrouter.HandleFunc("/recover", controllers.RecoverOrderHandler).Methods("POST")

	subrouter.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		ws.ServeWs(hub, w, r)
	})

	router.PathPrefix("/commerce/api/v2/order").Handler(negroni.New(
		negroni.HandlerFunc(common.Authorize),
		negroni.Wrap(orderRouter),
	))
	return router
}
