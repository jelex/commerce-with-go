package routers

import (
	"commerce/common"
	"commerce/controllers"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func SetIndexRoutes(router *mux.Router) *mux.Router {

	indexRouter := mux.NewRouter()

	indexRouter.HandleFunc("/commerce/api/v2/index", controllers.Index).Methods("GET")

	router.PathPrefix("/commerce/api/v2/index").Handler(negroni.New(
		negroni.HandlerFunc(common.Authorize),
		negroni.Wrap(indexRouter),
	))
	return router
}
