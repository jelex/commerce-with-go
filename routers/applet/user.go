package applet

import (
	"commerce/web"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

/* 此文件是 app 用户业务 请求 */

func SetUserRoutes(router *mux.Router) *mux.Router {

	userRouter := mux.NewRouter()
	subrouter := userRouter.PathPrefix("/commerce/user").Subrouter()
	subrouter.HandleFunc("/emailCode", web.SendEmailHandler).Methods("POST")
	subrouter.HandleFunc("/bindEmail", web.BindEmailHandler).Methods("POST")

	router.PathPrefix("/commerce/user").Handler(negroni.New(
		negroni.HandlerFunc(web.AppAuthorize),
		negroni.Wrap(userRouter),
	))
	return router
}
