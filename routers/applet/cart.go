package applet

import (
	"commerce/web"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

/* 此文件是 app购物车 请求 */

func SetCartRouters(router *mux.Router) *mux.Router {

	cartRouter := mux.NewRouter()
	subrouter := cartRouter.PathPrefix("/commerce/cart").Subrouter()
	subrouter.HandleFunc("/add", web.AddToCart).Methods("POST")
	subrouter.HandleFunc("/index", web.CartIndex).Methods("POST")
	subrouter.HandleFunc("/checked", web.SelectCartItemOrNot).Methods("POST")
	subrouter.HandleFunc("/update", web.UpdateCartItemCount).Methods("POST")
	subrouter.HandleFunc("/delete", web.DeleteCartItems).Methods("POST")
	subrouter.HandleFunc("/checkout", web.CheckOut).Methods("POST")

	router.PathPrefix("/commerce/cart").Handler(negroni.New(
		negroni.HandlerFunc(web.AppAuthorize),
		negroni.Wrap(cartRouter),
	))
	return router
}
