package applet

import (
	"commerce/web"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

/* 此文件是 app 帮助中心 请求 */

func SetHelpRoutes(router *mux.Router) *mux.Router {

	helpRouter := mux.NewRouter()
	subrouter := helpRouter.PathPrefix("/commerce/helpissue").Subrouter()
	subrouter.HandleFunc("/typeList", web.HelpTypeList).Methods("POST")
	subrouter.HandleFunc("/issueList", web.IssueList).Methods("POST")

	router.PathPrefix("/commerce/helpissue").Handler(negroni.New(
		//negroni.HandlerFunc(web.AppAuthorize),
		negroni.Wrap(helpRouter),
	))
	return router
}
