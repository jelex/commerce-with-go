package applet

import (
	"commerce/web"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

/* 此文件是 app地址 请求 */

func SetAddrRouters(router *mux.Router) *mux.Router {

	addrRouter := mux.NewRouter()
	subrouter := addrRouter.PathPrefix("/commerce/address").Subrouter()
	subrouter.HandleFunc("/list", web.ListAddr).Methods("POST")
	subrouter.HandleFunc("/detail", web.AddrDetail).Methods("POST")
	subrouter.HandleFunc("/regionlist", web.RegionList).Methods("POST")
	subrouter.HandleFunc("/save", web.SaveAddr).Methods("POST")
	subrouter.HandleFunc("/delete", web.DeleteAddr).Methods("POST")

	router.PathPrefix("/commerce/address").Handler(negroni.New(
		negroni.HandlerFunc(web.AppAuthorize),
		negroni.Wrap(addrRouter),
	))
	return router
}
