package applet

import (
	"commerce/web"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func SetIndexRoutes(router *mux.Router) *mux.Router {

	indexRouter := mux.NewRouter()

	subIndexRouter := indexRouter.PathPrefix("/commerce/index").Subrouter()
	subIndexRouter.HandleFunc("/channel", web.ListChannel).Methods("POST")
	// 新品首发
	subIndexRouter.HandleFunc("/newGoods", web.ListNewGoods).Methods("POST")
	subIndexRouter.HandleFunc("/category", web.ListCategory).Methods("POST")
	subIndexRouter.HandleFunc("/hotGoods", web.ListHotGoods).Methods("POST")
	subIndexRouter.HandleFunc("/banner", web.ListBanner).Methods("POST")
	subIndexRouter.HandleFunc("/brand", web.ListBrand).Methods("POST")

	router.PathPrefix("/commerce/index").Handler(negroni.New(
		negroni.Wrap(indexRouter),
	))

	authRouter := mux.NewRouter()

	authRouter.HandleFunc("/commerce/auth/login_by_wx", web.AuthByWx).Methods("POST")

	router.PathPrefix("/commerce/auth").Handler(negroni.New(
		negroni.Wrap(authRouter),
	))
	return router
}
