package applet

import (
	"commerce/web"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

/* 此文件是 app品牌 请求 */

func SetAppBrandRoutes(router *mux.Router) *mux.Router {

	aBrandRouter := mux.NewRouter()
	subrouter := aBrandRouter.PathPrefix("/commerce/brand").Subrouter()
	subrouter.HandleFunc("/list", web.BrandList).Methods("POST")
	subrouter.HandleFunc("/detail", web.BrandDetail).Methods("POST")

	router.PathPrefix("/commerce/brand").Handler(negroni.New(
		negroni.Wrap(aBrandRouter),
	))
	return router
}
