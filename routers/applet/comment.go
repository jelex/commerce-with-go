package applet

import (
	"commerce/web"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

/* 此文件是 app 评价 请求 */

func SetCommentRoutes(router *mux.Router) *mux.Router {

	commentRouter := mux.NewRouter()
	subrouter := commentRouter.PathPrefix("/commerce/comment").Subrouter()
	subrouter.HandleFunc("/post", web.PostCommentHandler).Methods("POST")

	router.PathPrefix("/commerce/comment").Handler(negroni.New(
		negroni.HandlerFunc(web.AppAuthorize),
		negroni.Wrap(commentRouter),
	))
	return router
}
