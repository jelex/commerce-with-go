package applet

import (
	"commerce/web"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

/* 此文件是 app 商品收藏 请求 */

func SetGoodsCollectRoutes(router *mux.Router) *mux.Router {

	collectRouter := mux.NewRouter()
	subrouter := collectRouter.PathPrefix("/commerce/collect").Subrouter()
	subrouter.HandleFunc("/collectOrNot", web.AddOrDeleteCollect).Methods("POST")
	subrouter.HandleFunc("/list", web.ListCollect).Methods("POST")

	router.PathPrefix("/commerce/collect").Handler(negroni.New(
		// 商品收藏 操作涉及到登录后的操作，需要登录认证授权
		negroni.HandlerFunc(web.AppAuthorize),
		negroni.Wrap(collectRouter),
	))
	return router
}
