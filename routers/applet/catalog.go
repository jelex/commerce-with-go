package applet

import (
	"commerce/web"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func SetCatalogRoutes(router *mux.Router) *mux.Router {

	catalogRouter := mux.NewRouter()
	subrouter := catalogRouter.PathPrefix("/commerce/catalog").Subrouter()
	subrouter.HandleFunc("/index", web.CatalogIndex).Methods("POST")
	subrouter.HandleFunc("/current", web.CurrentCategory).Methods("POST")

	router.PathPrefix("/commerce/catalog").Handler(negroni.New(
		negroni.Wrap(catalogRouter),
	))

	searchRouter := mux.NewRouter()

	searchRouter.HandleFunc("/commerce/search/index", web.SearchIndex).Methods("POST")
	searchRouter.HandleFunc("/commerce/search/helper", web.SearchHelper).Methods("POST")

	router.PathPrefix("/commerce/search").Handler(negroni.New(
		negroni.Wrap(searchRouter),
	))
	return router
}
