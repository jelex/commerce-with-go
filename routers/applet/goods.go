package applet

import (
	"commerce/web"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

/* 此文件是 app 商品 请求 */

func SetAppGoodsRoutes(router *mux.Router) *mux.Router {

	aGoodsRouter := mux.NewRouter()
	subrouter := aGoodsRouter.PathPrefix("/commerce/goods").Subrouter()
	subrouter.HandleFunc("/count", web.GoodsCount).Methods("POST")
	subrouter.HandleFunc("/category", web.GoodsCategory).Methods("POST")
	subrouter.HandleFunc("/list", web.GoodsList).Methods("POST")
	subrouter.HandleFunc("/detail", web.GoodsDetail).Methods("POST")
	subrouter.HandleFunc("/related", web.RelatedSkus).Methods("POST")
	subrouter.HandleFunc("/cartcount", web.CountGoods).Methods("POST")
	subrouter.HandleFunc("/commentList", web.GoodsCommentList).Methods("POST")
	subrouter.HandleFunc("/commentCount", web.CountGoodsComment).Methods("POST")

	router.PathPrefix("/commerce/goods").Handler(negroni.New(
		negroni.Wrap(aGoodsRouter),
	))

	goodsRouter := mux.NewRouter()
	subrouter2 := goodsRouter.PathPrefix("/commerce/buy").Subrouter()
	subrouter2.HandleFunc("/add", web.BuyToCart).Methods("POST")

	router.PathPrefix("/commerce/buy").Handler(negroni.New(
		negroni.HandlerFunc(web.AppAuthorize),
		negroni.Wrap(goodsRouter),
	))
	return router

	return router
}
