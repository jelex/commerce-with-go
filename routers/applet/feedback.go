package applet

import (
	"commerce/web"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

/* 此文件是 app 意见反馈 请求 */

func SetFeedbackRoutes(router *mux.Router) *mux.Router {

	feedbackRouter := mux.NewRouter()
	subrouter := feedbackRouter.PathPrefix("/commerce/feedback").Subrouter()
	subrouter.HandleFunc("/save", web.SaveFeedbackHandler).Methods("POST")

	router.PathPrefix("/commerce/feedback").Handler(negroni.New(
		negroni.HandlerFunc(web.AppAuthorize),
		negroni.Wrap(feedbackRouter),
	))
	return router
}
