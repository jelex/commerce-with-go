package applet

import (
	"commerce/web"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

/* 此文件是 app 订单&支付 请求 */

func SetOrderPayRouters(router *mux.Router) *mux.Router {

	orderPayRouter := mux.NewRouter()
	subrouter := orderPayRouter.PathPrefix("/commerce/order").Subrouter()
	subrouter.HandleFunc("/submit", web.SubmitOrder).Methods("POST")
	subrouter.HandleFunc("/list", web.ListOrder).Methods("POST")
	subrouter.HandleFunc("/detail", web.OrderDetail).Methods("POST")
	subrouter.HandleFunc("/cancelOrder", web.CancelOrder).Methods("POST")
	subrouter.HandleFunc("/confirmOrder", web.ConfirmOrderGoods).Methods("POST")

	router.PathPrefix("/commerce/order").Handler(negroni.New(
		negroni.HandlerFunc(web.AppAuthorize),
		negroni.Wrap(orderPayRouter),
	))
	return router
}
