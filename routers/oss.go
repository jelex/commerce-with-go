package routers

import (
	"commerce/controllers"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func SetOssRoute(router *mux.Router) *mux.Router {

	ossRouter := mux.NewRouter()

	ossRouter.HandleFunc("/commerce/api/v2/oss/policy", controllers.GetPolicyToken).Methods("GET")

	router.PathPrefix("/commerce/api/v2/oss").Handler(negroni.New(
		negroni.Wrap(ossRouter),
	))
	return router
}
