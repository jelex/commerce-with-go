package routers

import (
	"commerce/common"
	"commerce/controllers"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func SetCategoryRoutes(router *mux.Router) *mux.Router {

	indexRouter := mux.NewRouter()

	indexRouter.HandleFunc("/commerce/api/v2/category/list", controllers.ListCategory).Methods("GET")
	indexRouter.HandleFunc("/commerce/api/v2/category/addUI", controllers.AddUIHandler).Methods("GET")
	indexRouter.HandleFunc("/commerce/api/v2/category/updateUI", controllers.GetCategoryHandler).Methods("GET")
	indexRouter.HandleFunc("/commerce/api/v2/category/addOrEdit", controllers.AddOrEditCategoryHandler).Methods("POST")
	indexRouter.HandleFunc("/commerce/api/v2/category/delete", controllers.DeleteCategoryHandler).Methods("GET")
	indexRouter.HandleFunc("/commerce/api/v2/category/updateStatus", controllers.UpdateCategoryStatusHandler).Methods("POST")

	router.PathPrefix("/commerce/api/v2/category").Handler(negroni.New(
		negroni.HandlerFunc(common.Authorize),
		negroni.Wrap(indexRouter),
	))
	return router
}
