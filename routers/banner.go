package routers

import (
	"commerce/common"
	"commerce/controllers"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func SetBannerRoutes(router *mux.Router) *mux.Router {

	bannerRouter := mux.NewRouter()

	subrouter := bannerRouter.PathPrefix("/commerce/api/v2/banner").Subrouter()

	subrouter.HandleFunc("/list", controllers.ListBanner).Methods("GET")
	subrouter.HandleFunc("/addUI", controllers.AddBannerUIHandler).Methods("GET")
	subrouter.HandleFunc("/updateUI", controllers.GetBannerHandler).Methods("GET")
	subrouter.HandleFunc("/addOrEdit", controllers.AddOrEditBannerHandler).Methods("POST")
	subrouter.HandleFunc("/updateStatus", controllers.UpdateBannerStatusHandler).Methods("POST")

	router.PathPrefix("/commerce/api/v2/banner").Handler(negroni.New(
		negroni.HandlerFunc(common.Authorize),
		negroni.Wrap(bannerRouter),
	))
	return router
}
