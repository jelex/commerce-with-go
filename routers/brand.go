package routers

import (
	"commerce/common"
	"commerce/controllers"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func SetBrandRoutes(router *mux.Router) *mux.Router {

	brandRoute := mux.NewRouter()

	subrouter := brandRoute.PathPrefix("/commerce/api/v2/brand").Subrouter()

	subrouter.HandleFunc("/list", controllers.ListBrand).Methods("GET")
	subrouter.HandleFunc("/addUI", controllers.AddBrandUIHandler).Methods("GET")
	subrouter.HandleFunc("/updateUI", controllers.GetBrandHandler).Methods("GET")
	subrouter.HandleFunc("/addOrEdit", controllers.AddOrEditBrandHandler).Methods("POST")
	subrouter.HandleFunc("/updateStatus", controllers.UpdateBrandStatusHandler).Methods("POST")

	router.PathPrefix("/commerce/api/v2/brand").Handler(negroni.New(
		negroni.HandlerFunc(common.Authorize),
		negroni.Wrap(brandRoute),
	))
	return router
}
