package routers

import (
	"commerce/common"
	"commerce/controllers"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
)

func SetAdminFeedbackRoute(router *mux.Router) *mux.Router {

	orderRouter := mux.NewRouter()
	subrouter := orderRouter.PathPrefix("/commerce/api/v2/feedback").Subrouter()

	subrouter.HandleFunc("/list", controllers.ListFeedbackHandler).Methods("GET")
	subrouter.HandleFunc("/delete", controllers.DeleteFeedbackStatusHandler).Methods("POST")

	router.PathPrefix("/commerce/api/v2/feedback").Handler(negroni.New(
		negroni.HandlerFunc(common.Authorize),
		negroni.Wrap(orderRouter),
	))
	return router
}
