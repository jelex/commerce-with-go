package web

import (
	"commerce/common"
	"commerce/data"
	"commerce/vo"
	"encoding/json"
	"net/http"
	"strconv"
)

/* 此文件是 app 帮助中心 请求 */

func HelpTypeList(w http.ResponseWriter, r *http.Request) {

	issues, err := data.ListAllHelpIssue()
	if err != nil {
		common.Error.Printf("HelpTypeList err:%v", err)
		j, _ := json.Marshal(vo.Err(err.Error()))
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(j)
		return
	}
	var res []vo.HelpTypeVo
	for _, i := range issues {
		res = append(res, vo.HelpTypeVo{Id: i.Id, TypeName: i.TypeName})
	}

	j, _ := json.Marshal(vo.Ok(res))
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

func IssueList(w http.ResponseWriter, r *http.Request) {

	typeId, _ := strconv.Atoi(r.PostFormValue("typeId"))

	infos, err := data.ListHelpInfoByTypeId(typeId)
	if err != nil {
		common.Error.Printf("IssueList err:%v", err)
		j, _ := json.Marshal(vo.Err(err.Error()))
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(j)
		return
	}
	var res []vo.HelpInfoVo
	for _, i := range infos {
		res = append(res, vo.HelpInfoVo{Id: i.Id, Question: i.Question, Answer: i.Answer})
	}
	j, _ := json.Marshal(vo.Ok(res))
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}
