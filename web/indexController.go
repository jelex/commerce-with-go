package web

import (
	"commerce/common"
	"commerce/data"
	"commerce/model"
	"commerce/vo"
	"encoding/json"
	"net/http"
)

/* 此文件是首页请求 */

func ListChannel(w http.ResponseWriter, r *http.Request) {

	banner := vo.Channel{IconUrl: "https://commerce-hello.oss-cn-beijing.aliyuncs.com/keep_going.jpeg", Name: "keep going!"}
	banner2 := vo.Channel{IconUrl: "https://commerce-hello.oss-cn-beijing.aliyuncs.com/run.png", Name: "run!"}
	banner3 := vo.Channel{IconUrl: "https://commerce-hello.oss-cn-beijing.aliyuncs.com/fun.jpeg", Name: "fun!"}
	banner4 := vo.Channel{IconUrl: "https://commerce-hello.oss-cn-beijing.aliyuncs.com/dream.jpeg", Name: "dream!"}
	var channel []vo.Channel
	channel = append(channel, banner, banner2, banner3, banner4)

	j, _ := json.Marshal(vo.Ok(vo.IndexChannelVo{channel}))
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

func ListNewGoods(w http.ResponseWriter, r *http.Request) {

	var res vo.NewGoodsVo

	skuList, err := data.ListNewGoodsSku()
	if err != nil {
		common.Error.Printf("ListNewGoods err:%v", err)
		j, _ := json.Marshal(vo.Err(err.Error()))
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(j)
		return
	}
	var voList []vo.AppSku
	for _, k := range skuList {
		voList = append(voList, vo.AppSku{Id: k.Id, Name: k.Name, MainImg: k.MainImg, RetailPrice: k.RealPrice})
	}
	res.NewGoodsList = voList

	j, _ := json.Marshal(vo.Ok(res))
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

func ListCategory(w http.ResponseWriter, r *http.Request) {

	gs, err := data.ListHotGoodsIdList()
	if err != nil {
		common.Error.Printf("ListCategory ListHotGoodsIdList err:%v", err)
		j, _ := json.Marshal(vo.Err(err.Error()))
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(j)
		return
	}
	dbGoodsList, err := data.ListCategory(gs)
	if err != nil {
		common.Error.Printf("ListCategory err:%v", err)
		j, _ := json.Marshal(vo.Err(err.Error()))
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(j)
		return
	}
	if dbGoodsList == nil || len(dbGoodsList) == 0 {
		common.Error.Printf("no ListCategory ret...")
		j, _ := json.Marshal(vo.Err(err.Error()))
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(j)
		return
	}
	gcMap := make(map[int]model.Goods, len(dbGoodsList))
	var gIds []int
	for _, x := range dbGoodsList {
		gcMap[x.Id] = x
		gIds = append(gIds, x.Id)
	}
	skus, err := data.ListAppGoods(gIds, 1, 2)
	if err != nil {
		common.Error.Printf("2.ListCategory err:%v", err)
		j, _ := json.Marshal(vo.Err(err.Error()))
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(j)
		return
	}

	var res vo.IndexCategoryVo
	var categoryList []vo.SingleCategoryVo

	fMap := make(map[int]bool, len(dbGoodsList))
	for k, v := range gcMap {

		if h, ok := fMap[v.Id]; ok && h {
			continue
		}
		var c vo.SingleCategoryVo
		c.Id = v.CategoryId
		c.Name = v.CategoryName

		var skuList []vo.AppSku
		for _, sku := range skus {
			if sku.GoodsId == k {
				skuList = append(skuList, vo.AppSku{Id: sku.Id, Name: sku.Name, MainImg: sku.MainImg, RetailPrice: sku.RealPrice})
			}
		}
		c.GoodsList = skuList

		categoryList = append(categoryList, c)

		fMap[c.Id] = true
	}
	res.CategoryList = categoryList

	j, _ := json.Marshal(vo.Ok(res))
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

func ListHotGoods(w http.ResponseWriter, r *http.Request) {

	var res vo.IndexHotGoodsVo

	skuList, err := data.ListHotGoodsSku()
	if err != nil {
		common.Error.Printf("ListHotGoods err:%v", err)
		j, _ := json.Marshal(vo.Err(err.Error()))
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(j)
		return
	}
	var voList []vo.AppSku
	for _, k := range skuList {
		voList = append(voList, vo.AppSku{Id: k.Id, Name: k.Name, MainImg: k.MainImg, RetailPrice: k.RealPrice, Desc: k.Description})
	}
	res.HotGoodsList = voList

	j, _ := json.Marshal(vo.Ok(res))
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

func ListBanner(w http.ResponseWriter, r *http.Request) {

	var res vo.IndexBannerVo

	bannerList, err := data.ListBanner()
	if err != nil {
		common.Error.Printf("ListBanner err:%v", err)
		j, _ := json.Marshal(vo.Err(err.Error()))
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(j)
		return
	}
	var voList []vo.BannerVo
	for _, b := range bannerList {
		voList = append(voList, vo.BannerVo{Id: b.Id, Link: b.Link, ImgUrl: b.ImgUrl})
	}
	res.BannerList = voList

	j, _ := json.Marshal(vo.Ok(res))
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

func ListBrand(w http.ResponseWriter, r *http.Request) {

	var res vo.IndexBrandVo

	brandList, err := data.ListAppBrand()
	if err != nil {
		common.Error.Printf("ListBrand err:%v", err)
		j, _ := json.Marshal(vo.Err(err.Error()))
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(j)
		return
	}
	var list []vo.BrandVo
	for _, b := range brandList {
		brandVo := vo.BrandVo{Id: b.Id, Name: b.Name, ImgUrl: b.Logo, Description: b.Description}
		list = append(list, brandVo)
	}
	res.BrandList = list

	j, _ := json.Marshal(vo.Ok(res))
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}
