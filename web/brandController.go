package web

import (
	"commerce/cache"
	"commerce/common"
	"commerce/data"
	"commerce/model"
	"commerce/vo"
	"encoding/json"
	"net/http"
	"strconv"
	"time"
)

/* 此文件是 app 品牌 请求 */

func BrandDetail(w http.ResponseWriter, r *http.Request) {

	brandId, _ := strconv.Atoi(r.PostFormValue("id"))

	brand, err := data.GetBrandById(brandId)
	if err != nil {
		common.Error.Printf("BrandDetail err:%v", err)
		j, _ := json.Marshal(vo.Err(err.Error()))
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(j)
		return
	}
	var res vo.BrandDetailVo = vo.BrandDetailVo{
		Brand: vo.BrandVo{Id: brand.Id, Name: brand.Name, Description: brand.Description, ImgUrl: brand.Logo},
	}
	j, _ := json.Marshal(vo.Ok(res))
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

func BrandList(w http.ResponseWriter, r *http.Request) {

	categoryId, _ := strconv.Atoi(r.PostFormValue("categoryId"))
	page, _ := strconv.Atoi(r.PostFormValue("page"))
	size, _ := strconv.Atoi(r.PostFormValue("size"))
	sort := r.PostFormValue("sort")
	order := r.PostFormValue("order")
	keyword := r.PostFormValue("keyword")

	var pageRes *common.Page
	var categoryList []vo.CategoryNav
	// 按照商品类别 查询
	if categoryId > 0 {
		goodsIdList, err := data.ListGoodsByCategoryId(categoryId, 1)
		if err != nil {
			common.Error.Printf("GoodsList,categoryId:%d err:%v", categoryId, err)
			j, _ := json.Marshal(vo.Err(err.Error()))
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			w.Write(j)
			return
		}
		if goodsIdList == nil || len(goodsIdList) == 0 {
			common.Error.Printf("no GoodsList by categoryId:%d", categoryId)
			j, _ := json.Marshal(vo.Err(err.Error()))
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			w.Write(j)
			return
		}
		pageRes, err = data.PageAppGoods(goodsIdList, page, size)
		if err != nil {
			common.Error.Printf("2.GoodsList by categoryId:%d, err:%v", categoryId, err)
			j, _ := json.Marshal(vo.Err(err.Error()))
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			w.Write(j)
			return
		}
	} else {
		// 统计关键词搜索次数
		cache.ZIncrBy(common.HOT_KW_LIST, 10000000000+int(time.Now().Unix()), keyword, common.HOT_KW_LIMIT)

		// 模糊搜索-》提示得到 sku名称 搜索
		var err error
		pageRes, err = data.PageAppGoodsByName(keyword, sort, order, page, size, &categoryList)
		if err != nil {
			common.Error.Printf("3.GoodsList by skuName:%s, err:%v", keyword, err)
			j, _ := json.Marshal(vo.Err(err.Error()))
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			w.Write(j)
			return
		}
	}
	var res vo.AppGoodsVo
	res.TotalPages = pageRes.TotalPages
	res.CurrentPage = page
	res.PageSize = size

	if categoryId == 0 {
		res.FilterCategoryList = categoryList
	}

	var skuList []vo.AppSku
	for _, k := range (pageRes.List).([]model.Sku) {
		skuList = append(skuList, vo.AppSku{Name: k.Name, MainImg: k.MainImg, RetailPrice: k.RealPrice})
	}
	res.GoodsList = skuList

	j, _ := json.Marshal(vo.Ok(res))
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}
