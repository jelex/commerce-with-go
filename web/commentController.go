package web

import (
	"commerce/common"
	"commerce/req"
	"commerce/service"
	"commerce/utils"
	"commerce/vo"
	"encoding/json"
	"net/http"
)

/* 此文件是 app 评论 请求 */

func PostCommentHandler(w http.ResponseWriter, r *http.Request) {

	member := extractUserFromContext(r)
	if member == nil || member.Mid <= 0 {
		common.DisplayTokenErr(w, http.StatusUnauthorized)
		return
	}
	var commentReq req.PostCommentReq
	err := json.NewDecoder(r.Body).Decode(&commentReq)
	if err != nil {
		common.Error.Printf("PostCommentHandler err:%v", err)
		j, _ := json.Marshal(vo.Err(err.Error()))
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(j)
		return
	}
	commentReq.MemberIp = utils.ClientPublicIP(r)
	commentReq.MemberId = member.Mid
	commentReq.MemberNickName = member.Mnickname
	commentReq.MemberAvatar = member.Avatar

	err = service.PostComment(commentReq)
	if err != nil {
		common.Error.Printf("2.PostCommentHandler err:%v", err)
		j, _ := json.Marshal(vo.Err(err.Error()))
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(j)
		return
	}
	j, _ := json.Marshal(vo.Ok(nil))
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}
