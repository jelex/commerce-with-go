package service

import (
	"commerce/common"
	"commerce/data"
	"commerce/model"
	"commerce/req"
	"context"
	"fmt"
	"time"
)

func PostComment(commentReq req.PostCommentReq) error {

	orderId := commentReq.OrderId
	skuId := commentReq.SkuId

	order, err := data.GetOrderById(orderId)
	if err != nil {
		common.Error.Printf("PostComment -> GetOrderById(%d) err:%v", orderId, err)
		return err
	}
	if order.UserId != commentReq.MemberId {
		common.Error.Printf("you %d cannot comment other member's(%d) order!", commentReq.MemberId, order.UserId)
		return fmt.Errorf("不是您的订单，不能评价！")
	}
	// 确定能否评价
	if order.State != common.ORDER_STATE_RECEIVED {
		common.Error.Printf("you %d have not received the orderId:%d ,skuId:%d",
			commentReq.MemberId, orderId, skuId)
		return fmt.Errorf("请收货后再评价！")
	}
	orderItem, err := data.GetOrderItemByOrderIdAndSkuId(orderId, skuId)
	if err != nil {
		return err
	}
	// 确定不能重复评价
	if orderItem.Commented == 1 {
		common.Error.Printf("you %d have already commented orderId:%d ,skuId:%d",
			commentReq.MemberId, orderId, skuId)
		return fmt.Errorf("您已评价！")
	}
	comment := model.GoodsComment{SkuId: skuId, MemberNickName: commentReq.MemberNickName,
		MemberIp: commentReq.MemberIp, CreatedTime: time.Now().Add(8 * time.Hour),
		Content: commentReq.Content, MemberAvatar: commentReq.MemberAvatar, CommentType: 0,
		GoodsId: orderItem.SpuId, GoodsName: orderItem.SkuName, SkuSpecs: orderItem.SkuAttrValue,
	}
	fail := func(err error) error {
		return fmt.Errorf("PostComment: %v", err)
	}
	ctx := context.Background()
	tx, err := common.DB.BeginTx(ctx, nil)
	if err != nil {
		return fail(err)
	}
	/**
		Defer the transaction’s rollback. If the transaction succeeds,
	it will be committed before the function exits, making the deferred rollback call a no-op.
		If the transaction fails it won’t be committed,
	meaning that the rollback will be called as the function exits.
	*/
	defer tx.Rollback()
	_, err = data.AddGoodsComment(tx, comment)
	if err != nil {
		return fail(err)
	}
	err = data.UpdateOrderItemCommentStatus(tx, orderItem.Id, 1)
	if err != nil {
		return fail(err)
	}
	// 查询此订单下已评价商品的数量，以判断是否可以更改订单状态为 已完成
	allCommented, err := data.CountCommentedOrderItemByOrderId(tx, orderId)
	if err == nil && allCommented {
		err = data.UpdateOrderStatusTx(tx, orderId, common.ORDER_STATE_FINISHED)
		if err != nil {
			common.Error.Printf("PostComment->UpdateOrderStatus err:%v", err)
			return fail(err)
		}
	}
	// 成功，提交事务
	if err = tx.Commit(); err != nil {
		return fail(err)
	}
	return nil
}
