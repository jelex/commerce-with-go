# golang-commerce

#### 软件架构
 linux 安装docker, docker安装nginx,映射配置，端口，日志等目录；

外部请求通过https(443端口)进入到 nginx, nginx再转发到go server的9088端口服务。


go服务使用 negroni web工具.

mysql：使用linux上自己搭建的mysql 5.7

redis: 使用linux上自己搭建的redis

oss: 使用阿里云服务保存图片资源

email: 封装使用golang自身的邮件服务

------------------

cache : redis 缓存及存储功能模块

captcha: 验证码生成模块

common: 通用工具模块

controllers: 后台管理http handler入口

data: dao 操纵db模块

keys: 密钥文件

model: bean db 数据模型

req: http 请求入参数据模型

routers: 路由

service: 业务层

utils: 工具类

views: html页面视图

vo: 响应数据模型

web: c端http请求handler入口

main: 项目启动入口

 **想要登录成功，请务必在user表中插入一条记录，插入的值请查看
adminController.go中的代码** 

邮件工具中换成自己的账号信息: utils/mail.go
mysql + redis 换成自己的账号：common/config.json

部署可查看 nginx配置 目录下内容。

此系统中仅是后台功能代码，并且缺少支付相关代码以及还有bug，最新完整版功能，待合进来