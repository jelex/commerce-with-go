package utils

import (
	"commerce/common"
	"fmt"
	"net/smtp"
	"strings"
)

const addr = "smtp.qq.com:25"

var (
	auth = smtp.PlainAuth("", "xx@qq.com", "<pwd>", "smtp.qq.com")
	from = "xx@qq.com"
	tpl  = `From: yy@qq.com
To: %s
Subject: %s

%s
`
)

func Send(recipient string, subject, msg string) error {

	return SendWithContentType(recipient, subject, "text/plain", msg)
}

func SendWithContentType(recipient string, subject, contentType, msg string) error {

	rawMsg := []byte(strings.Replace(fmt.Sprintf(tpl, recipient, subject, contentType, msg), "\n", "\r\n", -1))
	err := smtp.SendMail(addr, auth, from, []string{recipient}, rawMsg)
	if err != nil {
		common.Error.Println(err)
	}
	return err
}
