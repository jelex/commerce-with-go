package cache

import (
	"fmt"
	"testing"
	"time"
)

func TestSet(t *testing.T) {

	err := Set("golang", "好语言！")
	fmt.Println(err)
}

func TestGet(t *testing.T) {

	result, err := Get("golang")
	fmt.Println(result, err)
}

func TestHSet(t *testing.T) {

	err := HSet("user", "email", "ss@ss.com")
	fmt.Println(err)
}

func TestHGet(t *testing.T) {

	fmt.Println(HGet("user", "email"))
}

func TestHGetAll(t *testing.T) {

	all, err := HGetAll("user")
	fmt.Println(all, err)
}

func TestZAddWithLimit(t *testing.T) {

	ZAddWithLimit("CS:19", int(time.Now().Unix()), "apple", 5)
	time.Sleep(1 * time.Second)
	ZAddWithLimit("CS:19", int(time.Now().Unix()), "小米", 5)
	time.Sleep(1 * time.Second)
	ZAddWithLimit("CS:19", int(time.Now().Unix()), "A家", 5)
}

func TestZIncrBy(t *testing.T) {

	fmt.Println(ZIncrBy("CS:19", 1, "apple", 0))
	fmt.Println(ZIncrBy("CS:19", 1, "apple2", 0))
	fmt.Println(ZIncrBy("CS:not-exist", 1, "apple", 0))
}

func TestZRange(t *testing.T) {

	list, err := ZRange("CS:19", 0, -1)
	fmt.Println(list, err)
}

func TestZRemoveAll(t *testing.T) {

	err := ZRemoveAll("CS:19")
	fmt.Println(err)
}

func TestSetWithTTL(t *testing.T) {
	fmt.Println(SetWithTTL("aaa", "bbb", 60))
}

func TestSetIfAbsent(t *testing.T) {

	fmt.Println(SetIfAbsent("aaa", "bb", 300))
}

func TestReleaseLock(t *testing.T) {

	fmt.Println(ReleaseLock("aaa", "bb", `if redis.call("get",KEYS[1]) == ARGV[1]
then
    return redis.call("del",KEYS[1])
else
    return 0
end`))
}

func TestDelete(t *testing.T) {
	// 1637152864
	//10000000000
	fmt.Println(int(time.Now().Unix()))
	fmt.Println(^uint(0) >> 1)
}
