package cache

import (
	"github.com/gomodule/redigo/redis"
)

var Pool *redis.Pool

func SetKV(prefix, k, v string) error {

	var key = k
	if len(prefix) > 0 {
		key = prefix + ":" + k
	}
	c := Pool.Get()
	defer c.Close()
	_, err := redis.String(c.Do("SET", key, v))
	if err != nil {
		return err
	}
	return nil
}

func Set(k, v string) error {

	return SetKV("", k, v)
}

func SetWithTTL(k, v string, ttl int) error {

	c := Pool.Get()
	defer c.Close()
	_, err := redis.String(c.Do("SETEX", k, ttl, v))
	return err
}

func Get(k string) (string, error) {

	c := Pool.Get()
	defer c.Close()
	s, err := redis.String(c.Do("GET", k))
	if err != nil {
		return "", err
	}
	return s, nil
}

func Delete(k string) (string, error) {

	c := Pool.Get()
	defer c.Close()
	s, err := redis.String(c.Do("DEL", k))
	if err != nil {
		return "", err
	}
	return s, nil
}

func HSet(hashKey, key, value string) error {

	c := Pool.Get()
	defer c.Close()
	_, err := c.Do("HSET", hashKey, key, value)
	return err
}

func HGet(hashKey, key string) (string, error) {

	c := Pool.Get()
	defer c.Close()
	return redis.String(c.Do("HGET", hashKey, key))
}

func HGetAll(hashKey string) (map[string]string, error) {

	c := Pool.Get()
	defer c.Close()
	return redis.StringMap(c.Do("HGETALL", hashKey))
}

func HDel(hashKey, key string) (err error) {

	c := Pool.Get()
	defer c.Close()
	_, err = c.Do("HDEL", hashKey, key)
	return
}

func TTL(k string) (int, error) {

	c := Pool.Get()
	defer c.Close()
	ttl, err := redis.Int64(c.Do("TTL", k))
	if err != nil {
		return 0, err
	}
	return int(ttl), nil
}

func SetTTL(k string, ttl int) error {

	c := Pool.Get()
	defer c.Close()
	_, err := c.Do("EXPIRE", k, ttl)

	return err
}

func ZAddWithLimit(key string, score int, member string, limit int) error {

	conn := Pool.Get()
	defer conn.Close()
	_, err := conn.Do("ZADD", key, score, member)
	if err != nil {
		return err
	}
	count, _ := redis.Int(conn.Do("ZCARD", key))
	if count > limit {
		_, err = conn.Do("ZREMRANGEBYRANK", key, 0, 0)
	}
	return err
}

func ZIncrBy(key string, increment int, member string, limit int) error {

	conn := Pool.Get()
	defer conn.Close()
	_, err := conn.Do("ZINCRBY", key, increment, member)
	if err != nil {
		return err
	}
	if limit > 0 {
		count, _ := redis.Int(conn.Do("ZCARD", key))
		if count > limit {
			_, err = conn.Do("ZREMRANGEBYRANK", key, 0, 0)
		}
	}
	return err
}

func ZRange(key string, start, stop int) ([]string, error) {

	c := Pool.Get()
	defer c.Close()
	list, err := redis.Strings(c.Do("ZREVRANGE", key, start, stop))
	return list, err
}
func ZRemoveAll(key string) error {

	c := Pool.Get()
	defer c.Close()
	_, err := c.Do("ZREMRANGEBYRANK", key, 0, -1)
	return err
}

func SetIfAbsent(k, v string, timeout int) bool {

	c := Pool.Get()
	defer c.Close()
	reply, err := redis.String(c.Do("SET", k, v, "EX", timeout, "NX"))

	return err == nil && "OK" == reply
}

func ReleaseLock(k, v, script string) (interface{}, error) {

	c := Pool.Get()
	defer c.Close()

	return redis.Int(c.Do("EVAL", script, 1, k, v))
}
