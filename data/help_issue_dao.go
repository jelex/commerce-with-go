package data

import (
	"commerce/common"
	"commerce/model"
	"database/sql"
	"fmt"
)

func PageHelpIssue(pageNo, pageSize int, name string) (*common.Page, error) {

	whereSql := composeSearchQuerySql(name)

	row := common.DB.QueryRow("select count(*) FROM help_issue" + whereSql)
	var totalRecords int
	if err := row.Scan(&totalRecords); err != nil {
		return nil, err
	}
	stmt, err := common.DB.Prepare("select id, name, sort, is_valid FROM help_issue" + whereSql + " LIMIT ?, ?")
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query((pageNo-1)*pageSize, pageSize)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var hs []model.HelpIssue
	for rows.Next() {
		h := model.HelpIssue{}
		if err := rows.Scan(&h.Id, &h.TypeName, &h.Sort, &h.IsValid); err != nil {
			return nil, fmt.Errorf("问题中心分页数据有误:%s", err.Error())
		}
		hs = append(hs, h)
	}
	return common.NewPage(hs, pageNo, pageSize, totalRecords), nil
}

func GetHelpIssueById(id int) (*model.HelpIssue, error) {

	stmt, err := common.DB.Prepare("select id, name, sort FROM help_issue where id = ?")
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query(id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var h model.HelpIssue
	for rows.Next() {
		if err := rows.Scan(&h.Id, &h.TypeName, &h.Sort); err != nil {
			return nil, fmt.Errorf("help issue id查询数据有误:%s", err.Error())
		}
	}
	return &h, nil
}

func AddHelpIssue(h model.HelpIssue) (int, error) {

	stmt, err := common.DB.Prepare(`insert into help_issue(name, sort, is_valid) VALUES (?, ?, ?)`)
	if err != nil {
		return 0, err
	}
	defer stmt.Close()
	result, err := stmt.Exec(h.TypeName, h.Sort, h.IsValid)

	if err != nil {
		return 0, fmt.Errorf("save help issue err: %v", err)
	}
	id, err := result.LastInsertId()

	if err != nil {
		return 0, fmt.Errorf("save help issue insert id err: %v", err)
	}
	return int(id), nil
}

func UpdateHelpIssue(h model.HelpIssue) (int, error) {

	stmt, err := common.DB.Prepare("update help_issue set `name`=?, sort = ? WHERE id = ?")
	if err != nil {
		return 0, err
	}
	defer stmt.Close()

	var result sql.Result
	result, err = stmt.Exec(h.TypeName, h.Sort, h.Id)
	if err != nil {
		return 0, fmt.Errorf("update help issue err:%v", err.Error())
	}
	rowsAffected, err := result.RowsAffected()
	return int(rowsAffected), err
}

func UpdateHelpIssueStatus(id int, isValid int8) error {

	stmt, err := common.DB.Prepare("update help_issue set is_valid = ? WHERE id = ?")
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(isValid, id)

	return err
}

// ----------------app 专用接口开始-----------------------

func ListAllHelpIssue() ([]model.HelpIssue, error) {

	stmt, err := common.DB.Prepare("select id,  name FROM help_issue where is_valid = 1 order by sort desc")
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var hs []model.HelpIssue
	for rows.Next() {
		h := model.HelpIssue{}
		if err := rows.Scan(&h.Id, &h.TypeName); err != nil {
			return nil, fmt.Errorf("问题中心所有有效数据有误:%s", err.Error())
		}
		hs = append(hs, h)
	}
	return hs, nil
}
