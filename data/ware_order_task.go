package data

import (
	"commerce/common"
	"commerce/model"
	"database/sql"
	"fmt"
	"time"
)

func AddWareOrderTask(tx *sql.Tx, task model.WareOrderTask) (int, error) {

	sqlTpl := "insert into ware_order_task(order_id, order_sn, task_status, created_time) VALUES (?, ?, ?, ?)"

	stmt, err := tx.Prepare(sqlTpl)
	if err != nil {
		return 0, err
	}
	defer stmt.Close()
	result, err := stmt.Exec(task.OrderId, task.OrderNo, task.TaskStatus, time.Now().Add(8*time.Hour))

	if err != nil {
		return 0, err
	}
	id, err := result.LastInsertId()

	if err != nil {
		return 0, fmt.Errorf("AddWareOrderTask err: %v", err)
	}
	return int(id), nil
}

func AddWareOrderTaskDetail(tx *sql.Tx, td model.WareOrderTaskDetail) (int, error) {

	sqlTpl := "insert into ware_order_task_detail(sku_id, sku_name, sku_num, task_id, ware_id, lock_status) VALUES (?,?,?,?,?,?)"
	stmt, err := tx.Prepare(sqlTpl)

	if err != nil {
		return 0, err
	}
	defer stmt.Close()

	result, err := stmt.Exec(td.SkuId, td.SkuName, td.SkuNum, td.TaskId, td.WareId, td.LockStatus)

	if err != nil {
		return 0, err
	}
	id, err := result.LastInsertId()

	return int(id), nil
}

func GetWareOrderTaskByOrderId(orderId, taskStatus int) (*model.WareOrderTask, error) {

	stmt, err := common.DB.Prepare("SELECT id FROM ware_order_task where order_id = ? and task_status = ?")
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	var o model.WareOrderTask
	if err = stmt.QueryRow(orderId, taskStatus).Scan(&o.Id); err != nil {
		return nil, err
	}
	return &o, nil
}

func GetWareOrderTaskById(taskId, taskStatus int) (*model.WareOrderTask, error) {

	stmt, err := common.DB.Prepare("SELECT id, order_id FROM ware_order_task where id = ? and task_status = ?")
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	var o model.WareOrderTask
	if err = stmt.QueryRow(taskId, taskStatus).Scan(&o.Id, &o.OrderId); err != nil {
		return nil, err
	}
	return &o, nil
}

func ListWareOrderTaskDetailByTaskId(taskId, lockStatus int) ([]model.WareOrderTaskDetail, error) {

	stmt, err := common.DB.Prepare("select id, sku_id, sku_num, ware_id FROM ware_order_task_detail where task_id = ? AND lock_status = ?")
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query(taskId, lockStatus)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var ds []model.WareOrderTaskDetail
	for rows.Next() {
		d := model.WareOrderTaskDetail{}
		if err := rows.Scan(&d.Id, &d.SkuId, &d.SkuNum, &d.WareId); err != nil {
			return nil, fmt.Errorf("库存工作单明细表数据有误:%s", err.Error())
		}
		ds = append(ds, d)
	}
	return ds, nil
}

func UpdateWareOrderTaskDetailStatus(taskDetailId, lockStatus int) error {

	stmt, err := common.DB.Prepare("update ware_order_task_detail set lock_status = ? WHERE id = ?")
	if err != nil {
		return err
	}
	defer stmt.Close()

	result, err := stmt.Exec(lockStatus, taskDetailId)
	if err != nil {
		return fmt.Errorf("UpdateWareOrderTaskDetailStatus err:%v", err.Error())
	}
	_, err = result.RowsAffected()
	return err
}

func UpdateWareOrderTaskStatus(taskId int, taskStatus int8) error {

	stmt, err := common.DB.Prepare("update ware_order_task set task_status = ? WHERE id = ?")
	if err != nil {
		return err
	}
	defer stmt.Close()

	result, err := stmt.Exec(taskStatus, taskId)
	if err != nil {
		return fmt.Errorf("UpdateWareOrderTaskStatus err:%v", err.Error())
	}
	_, err = result.RowsAffected()
	return err
}

//func GetWareOrderDetailById(detailId, lockStatus int) (*model.WareOrderTaskDetail, error) {
//
//	stmt, err := common.DB.Prepare("select id, sku_id, sku_num, ware_id, task_id FROM ware_order_task_detail where id = ? AND lock_status = ?")
//	if err != nil {
//		return nil, err
//	}
//	defer stmt.Close()
//
//	rows, err := stmt.Query(detailId, lockStatus)
//	if err != nil {
//		return nil, err
//	}
//	defer rows.Close()
//
//	var d model.WareOrderTaskDetail
//	if err = stmt.QueryRow(detailId, lockStatus).Scan(&d.Id, &d.SkuId, &d.SkuNum, &d.WareId, &d.TaskId); err != nil {
//		return nil, err
//	}
//	return &d, nil
//}
