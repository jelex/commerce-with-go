package data

import (
	"commerce/common"
	"commerce/model"
	"fmt"
)

func GetMemberReceiveAddrById(id int) (*model.MemberReceiveAddr, error) {

	sqlTpl := "select id, name, phone, province_id, province, city_id, city, region_id, region, detail_address, default_status FROM member_receive_address where id = ?"

	stmt, err := common.DB.Prepare(sqlTpl)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	var m model.MemberReceiveAddr
	if err = stmt.QueryRow(id).Scan(&m.Id, &m.Name, &m.Phone, &m.ProvinceId, &m.Province,
		&m.CityId, &m.City, &m.RegionId, &m.Region, &m.DetailAddr, &m.DefaultStatus); err != nil {
		return nil, err
	}
	return &m, nil
}

func ListMemberReceiveAddr(memberId int) ([]model.MemberReceiveAddr, error) {

	stmt, err := common.DB.Prepare("select id, name, phone, province, city, region, detail_address, default_status FROM member_receive_address where member_id = ?")
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	rows, err := stmt.Query(memberId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var ms []model.MemberReceiveAddr
	for rows.Next() {
		m := model.MemberReceiveAddr{}
		if err := rows.Scan(&m.Id, &m.Name, &m.Phone, &m.Province, &m.City, &m.Region, &m.DetailAddr, &m.DefaultStatus); err != nil {
			return nil, fmt.Errorf("ListMemberReceiveAddr数据有误:%s", err.Error())
		}
		ms = append(ms, m)
	}
	return ms, nil
}

func AddMemberReceiveAddr(m model.MemberReceiveAddr) (int, error) {

	sqlTpl := "insert into member_receive_address(member_id, name, phone, province_id, province, city_id, city, region_id, region, detail_address, default_status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
	result, err := common.DB.Exec(sqlTpl, m.MemberId, m.Name, m.Phone, m.ProvinceId, m.Province, m.CityId, m.City, m.RegionId, m.Region, m.DetailAddr, m.DefaultStatus)

	if err != nil {
		return 0, err
	}
	id, err := result.LastInsertId()

	if err != nil {
		return 0, fmt.Errorf("save member receive addr insert id err: %v", err)
	}
	if m.DefaultStatus == 1 {
		// 删除其它地址的默认状态
		err = updateMemberReceiveAddrDefaultStatusById(m.MemberId, int(id), 0)
		if err != nil {
			common.Error.Println("updateMemberReceiveAddrDefaultStatusById:", err)
		}
	}
	return int(id), nil
}

func DeleteAddr(id int, memberId int) error {

	stmt, err := common.DB.Prepare("delete from member_receive_address where id = ? and member_id = ?")
	if err != nil {
		return nil
	}
	defer stmt.Close()

	_, err = stmt.Exec(id, memberId)

	return err
}

func UpdateMemberReceiveAddr(m model.MemberReceiveAddr) (int, error) {

	sqlTpl := "update member_receive_address set name = ?, phone = ?, province_id = ?, province = ?, city_id = ?, city = ?, region_id = ?, region = ?, detail_address = ?, default_status = ? where id = ? and member_id = ?"
	stmt, err := common.DB.Prepare(sqlTpl)
	if err != nil {
		return 0, nil
	}
	defer stmt.Close()

	result, err := stmt.Exec(m.Name, m.Phone, m.ProvinceId, m.Province, m.CityId, m.City, m.RegionId, m.Region, m.DetailAddr, m.DefaultStatus, m.Id, m.MemberId)

	if err != nil {
		return 0, err
	}
	id, err := result.LastInsertId()

	if err != nil {
		return 0, fmt.Errorf("update member receive addr insert id err: %v", err)
	}
	if m.DefaultStatus == 1 {
		// 更新其它地址的默认状态为 非默认
		err = updateMemberReceiveAddrDefaultStatusById(m.MemberId, m.Id, 0)
		if err != nil {
			common.Error.Println("updateMemberReceiveAddrDefaultStatusById:", err)
		}
	}
	return int(id), nil
}

func updateMemberReceiveAddrDefaultStatusById(memberId, excludeId, status int) error {

	sqlTpl := "update member_receive_address set default_status = ? where member_id = ? and id != ?"

	stmt, err := common.DB.Prepare(sqlTpl)
	if err != nil {
		return err
	}
	defer stmt.Close()

	result, err := stmt.Exec(status, memberId, excludeId)
	if err != nil {
		return fmt.Errorf("update member receiver addr info err:%v", err.Error())
	}
	_, err = result.RowsAffected()
	return err
}
