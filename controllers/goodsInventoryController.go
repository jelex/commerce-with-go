package controllers

import (
	"commerce/common"
	"commerce/data"
	"commerce/model"
	"html/template"
	"net/http"
	"path/filepath"
	"strconv"
	"strings"
)

func init() {

	common.Templates["goodsInventory"] = template.Must(template.ParseFiles(filepath.Join(common.CurDir, "views/templates/goodsInventory.html")))
	common.Templates["goodsInventory_edit"] = template.Must(template.ParseFiles(filepath.Join(common.CurDir, "views/templates/goodsInventory_edit.html")))
}

func GetStockBySkuId(w http.ResponseWriter, r *http.Request) {

	skuId, err := strconv.Atoi(r.FormValue("skuId"))
	if err != nil {
		common.Warn.Println("GetStockBySkuId invalid skuId param:", err)
		common.Templates["5xx"].Execute(w, err.Error())
		return
	}
	inventory, err := data.GetGoodsInventoryBySkuId(skuId)
	if err != nil {
		common.Templates["5xx"].Execute(w, nil)
		return
	}
	var list []model.GoodsInventory
	var totalRecords = 0
	if inventory != nil {
		list = append(list, *inventory)
		totalRecords = 1
	}
	var page = common.NewPage(list, 1, common.PAGE_SIZE, totalRecords)
	if page == nil {
		common.Templates["5xx"].Execute(w, nil)
		return
	}
	var qs = r.RequestURI

	index := strings.Index(r.RequestURI, "&pageNo=")
	// 这一步操作是点击分页来的
	if index != -1 {
		qs = r.RequestURI[:index]
	}
	page.Url = qs

	common.Templates["goodsInventory"].Execute(w, page)
}

func ListGoodsInventory(w http.ResponseWriter, r *http.Request) {

	pageNo, err := strconv.Atoi(r.FormValue("pageNo"))
	if err != nil {
		pageNo = 1
	}
	pageSize, err := strconv.Atoi(r.FormValue("pageSize"))
	if err != nil {
		pageSize = common.PAGE_SIZE
	}

	var page, _ = data.PageInventory(pageNo, pageSize)

	if page == nil {
		common.Templates["5xx"].Execute(w, nil)
		return
	}
	var qs = r.RequestURI

	index := strings.Index(r.RequestURI, "&pageNo=")
	// 这一步操作是点击分页来的
	if index != -1 {
		qs = r.RequestURI[:index]
	}
	page.Url = qs

	common.Templates["goodsInventory"].Execute(w, page)
}

func AddGoodsInventoryUIHandler(w http.ResponseWriter, r *http.Request) {

	var i model.GoodsInventory

	allSku, err := data.ListAllSku()
	if err == nil {
		i.AllSkus = allSku
	}
	common.Templates["goodsInventory_edit"].Execute(w, i)
}

func GetGoodsInventoryHandler(w http.ResponseWriter, r *http.Request) {

	id, err := strconv.Atoi(r.FormValue("id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	c, err := data.GetGoodsInventoryById(id)
	if err != nil {
		common.Templates["5xx"].Execute(w, err)
		return
	}
	allSku, err := data.ListAllSku()
	if err == nil {
		c.AllSkus = allSku
	}
	common.Templates["goodsInventory_edit"].Execute(w, c)
}

func AddOrEditGoodsInventoryHandler(w http.ResponseWriter, r *http.Request) {

	var id = 0
	idVal := r.PostFormValue("id")
	if len(idVal) > 0 {
		id, _ = strconv.Atoi(idVal)
	}
	skuId, _ := strconv.Atoi(r.PostFormValue("skuId"))
	sales, _ := strconv.Atoi(r.PostFormValue("sales"))
	boughtUserNum, _ := strconv.Atoi(r.PostFormValue("boughtUserNum"))
	stock, _ := strconv.Atoi(r.PostFormValue("stock"))
	viewed, _ := strconv.Atoi(r.PostFormValue("viewed"))

	b := model.GoodsInventory{Id: id, SkuId: skuId, Sales: sales, BoughtUserNum: boughtUserNum, Stock: stock, Viewed: viewed}
	if id <= 0 {
		id, _ = data.AddGoodsInventory(b)
	} else {
		id, _ = data.UpdateGoodsInventory(b)
	}
	//重定向到列表
	http.Redirect(w, r, "/commerce/api/v2/inventory/list", http.StatusFound)
}
