package controllers

import (
	"commerce/utils"
	"io"
	"net/http"
)

func GetPolicyToken(w http.ResponseWriter, r *http.Request) {

	response := utils.GetPolicyToken()
	w.Header().Set("Access-Control-Allow-Methods", "POST")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	io.WriteString(w, response)
}
