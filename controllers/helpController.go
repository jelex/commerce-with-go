package controllers

import (
	"commerce/common"
	"commerce/data"
	"commerce/model"
	"html/template"
	"net/http"
	"path/filepath"
	"strconv"
	"strings"
)

func init() {

	common.Templates["help_issue"] = template.Must(template.ParseFiles(filepath.Join(common.CurDir, "views/templates/help_issue.html")))
	common.Templates["help_issue_edit"] = template.Must(template.ParseFiles(filepath.Join(common.CurDir, "views/templates/help_issue_edit.html")))

	common.Templates["help_info"] = template.Must(template.ParseFiles(filepath.Join(common.CurDir, "views/templates/help_info.html")))
	common.Templates["help_info_edit"] = template.Must(template.ParseFiles(filepath.Join(common.CurDir, "views/templates/help_info_edit.html")))
}

func ListHelpIssueHandler(w http.ResponseWriter, r *http.Request) {

	pageNo, err := strconv.Atoi(r.FormValue("pageNo"))
	if err != nil {
		pageNo = 1
	}
	pageSize, err := strconv.Atoi(r.FormValue("pageSize"))
	if err != nil {
		pageSize = common.PAGE_SIZE
	}

	name := strings.TrimSpace(r.FormValue("name"))

	var page, _ = data.PageHelpIssue(pageNo, pageSize, name)

	if page == nil {
		common.Templates["5xx"].Execute(w, nil)
		return
	}
	// 分页条件查询使用
	var qs = r.URL.RawQuery

	index := strings.Index(r.RequestURI, "&pageNo=")
	var flag bool
	// 这一步操作是点击分页来的
	if index != -1 {
		qs = r.RequestURI[:index]
		flag = true
	} else {
		// 首次点击搜索按钮查询
		flag = false
		qs = "?"
		if len(name) > 0 {
			qs += "name=" + name
		}
	}
	if flag {
		page.Url = qs
	} else {
		page.Url = r.RequestURI + qs
	}

	common.Templates["help_issue"].Execute(w, page)
}

func AddHelpIssueUIHandler(w http.ResponseWriter, r *http.Request) {

	common.Templates["help_issue_edit"].Execute(w, nil)
}

func GetHelpIssueHandler(w http.ResponseWriter, r *http.Request) {

	id, err := strconv.Atoi(r.FormValue("id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	h, err := data.GetHelpIssueById(id)
	if err != nil {
		common.Templates["5xx"].Execute(w, err.Error())
		return
	}
	common.Templates["help_issue_edit"].Execute(w, h)
}

func AddOrEditHelpIssueHandler(w http.ResponseWriter, r *http.Request) {

	var id = 0
	idVal := r.PostFormValue("id")
	if len(idVal) > 0 {
		id, _ = strconv.Atoi(idVal)
	}
	name := r.PostFormValue("name")
	sort, _ := strconv.Atoi(r.PostFormValue("sort"))
	h := model.HelpIssue{Id: id, TypeName: name, Sort: int8(sort), IsValid: 0}
	var err error
	if id <= 0 {
		id, err = data.AddHelpIssue(h)
	} else {
		id, err = data.UpdateHelpIssue(h)
	}
	if err != nil {
		common.Error.Println("err help issue:", err)
	}
	//重定向到列表
	http.Redirect(w, r, "/commerce/api/v2/help/list", http.StatusFound)
}

func UpdateHelpIssueStatusHandler(w http.ResponseWriter, r *http.Request) {

	id, err := strconv.Atoi(r.FormValue("id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	status, err := strconv.Atoi(r.FormValue("status"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	data.UpdateHelpIssueStatus(id, int8(status))

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
}

func ListHelpInfoHandler(w http.ResponseWriter, r *http.Request) {

	pageNo, err := strconv.Atoi(r.FormValue("pageNo"))
	if err != nil {
		pageNo = 1
	}
	pageSize, err := strconv.Atoi(r.FormValue("pageSize"))
	if err != nil {
		pageSize = common.PAGE_SIZE
	}

	name := strings.TrimSpace(r.FormValue("name"))

	var page, _ = data.PageHelpInfo(pageNo, pageSize, name)

	if page == nil {
		common.Templates["5xx"].Execute(w, nil)
		return
	}
	// 分页条件查询使用
	var qs = r.URL.RawQuery

	index := strings.Index(r.RequestURI, "&pageNo=")
	var flag bool
	// 这一步操作是点击分页来的
	if index != -1 {
		qs = r.RequestURI[:index]
		flag = true
	} else {
		// 首次点击搜索按钮查询
		flag = false
		qs = "?"
		if len(name) > 0 {
			qs += "name=" + name
		}
	}
	if flag {
		page.Url = qs
	} else {
		page.Url = r.RequestURI + qs
	}

	common.Templates["help_info"].Execute(w, page)
}

func AddHelpInfoUIHandler(w http.ResponseWriter, r *http.Request) {

	issues, _ := data.ListAllHelpIssue()

	common.Templates["help_info_edit"].Execute(w, model.HelpInfo{AllTypeList: issues})
}

func GetHelpInfoHandler(w http.ResponseWriter, r *http.Request) {

	id, err := strconv.Atoi(r.FormValue("id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	h, err := data.GetHelpInfoById(id)
	if err != nil {
		common.Templates["5xx"].Execute(w, err.Error())
		return
	}
	issues, err := data.ListAllHelpIssue()
	if err != nil {
		common.Templates["5xx"].Execute(w, err.Error())
		return
	}
	h.AllTypeList = issues

	common.Templates["help_info_edit"].Execute(w, h)
}

func AddOrEditHelpInfoHandler(w http.ResponseWriter, r *http.Request) {

	var id = 0
	idVal := r.PostFormValue("id")
	if len(idVal) > 0 {
		id, _ = strconv.Atoi(idVal)
	}
	typeId, err := strconv.Atoi(r.PostFormValue("helpType"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	question := r.PostFormValue("question")
	answer := r.PostFormValue("answer")
	sort, _ := strconv.Atoi(r.PostFormValue("sort"))
	h := model.HelpInfo{Id: id, TypeId: typeId, Question: question, Answer: answer, Sort: int8(sort), IsValid: 0}
	if id <= 0 {
		id, err = data.AddHelpInfo(h)
	} else {
		id, err = data.UpdateHelpInfo(h)
	}
	if err != nil {
		common.Error.Println("err help info:", err)
	}
	//重定向到列表
	http.Redirect(w, r, "/commerce/api/v2/help/qaList", http.StatusFound)
}

func UpdateHelpInfoStatusHandler(w http.ResponseWriter, r *http.Request) {

	id, err := strconv.Atoi(r.FormValue("id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	status, err := strconv.Atoi(r.FormValue("status"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	data.UpdateHelpInfoStatus(id, int8(status))

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
}
