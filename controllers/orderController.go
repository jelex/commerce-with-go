package controllers

import (
	"commerce/common"
	"commerce/data"
	"commerce/service"
	"commerce/vo"
	"html/template"
	"net/http"
	"path/filepath"
	"strconv"
	"strings"
)

func init() {

	common.Templates["order"] = template.Must(template.ParseFiles(filepath.Join(common.CurDir, "views/templates/order.html")))
	common.Templates["order_edit"] = template.Must(template.ParseFiles(filepath.Join(common.CurDir, "views/templates/order_detail.html")))
}

func ListOrderHandler(w http.ResponseWriter, r *http.Request) {

	pageNo, err := strconv.Atoi(r.FormValue("pageNo"))
	if err != nil {
		pageNo = 1
	}
	pageSize, err := strconv.Atoi(r.FormValue("pageSize"))
	if err != nil {
		pageSize = common.PAGE_SIZE
	}

	name := strings.TrimSpace(r.FormValue("name"))

	var page, _ = data.ListV2Order(pageNo, pageSize, name)

	if page == nil {
		common.Templates["5xx"].Execute(w, nil)
		return
	}
	// 分页条件查询使用
	var qs = r.URL.RawQuery

	index := strings.Index(r.RequestURI, "&pageNo=")
	var flag bool
	// 这一步操作是点击分页来的
	if index != -1 {
		qs = r.RequestURI[:index]
		flag = true
	} else {
		// 首次点击搜索按钮查询
		flag = false
		qs = "?"
		if len(name) > 0 {
			qs += "name=" + name
		}
	}
	if flag {
		page.Url = qs
	} else {
		page.Url = r.RequestURI + qs
	}

	common.Templates["order"].Execute(w, page)
}

func GetOrderDetailHandler(w http.ResponseWriter, r *http.Request) {

	id, err := strconv.Atoi(r.FormValue("id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	var detailVo vo.OrderDetailPageVo

	detailVo, err = service.GetOrderDetail(id)
	if err != nil {
		common.Templates["5xx"].Execute(w, err.Error())
		return
	}
	common.Templates["order_edit"].Execute(w, detailVo)
}

func DeleteOrderHandler(w http.ResponseWriter, r *http.Request) {

	id, err := strconv.Atoi(r.FormValue("id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	data.UpdateOrderValid(id, 0)

	common.DisplayOk(w, "废弃成功!", http.StatusOK)
}

func RecoverOrderHandler(w http.ResponseWriter, r *http.Request) {

	id, err := strconv.Atoi(r.FormValue("id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	data.UpdateOrderValid(id, 1)

	common.DisplayOk(w, "恢复成功!", http.StatusOK)
}
