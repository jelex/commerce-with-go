package controllers

import (
	"commerce/common"
	"commerce/vo"
	"html/template"
	"net/http"
	"path/filepath"
)

func init() {

	//common.Templates["index"] = template.Must(template.ParseFiles(filepath.Join(common.CurDir, "views/index.html", filepath.Join(common.CurDir, "views/base.html")))
	common.Templates["index"] = template.Must(template.ParseFiles(filepath.Join(common.CurDir, "views/index.html")))
}

func Index(w http.ResponseWriter, r *http.Request) {

	jwtUser := r.Context().Value(common.JWT)

	//jwtUser := context.Get(r, common.JWT)
	if jwtUser == nil {
		http.Redirect(w, r, "/login.html", http.StatusFound)
		return
	}
	user, ok := jwtUser.(*common.JwtUser)
	if !ok {
		http.Redirect(w, r, "/login.html", http.StatusFound)
		return
	}
	var indexVo vo.IndexVo
	indexVo.Username = user.Uname
	indexVo.NewOrderNum = 0

	common.Templates["index"].Execute(w, indexVo)
}
