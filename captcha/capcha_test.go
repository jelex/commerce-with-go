package captcha

import (
	"bufio"
	"commerce/common"
	"os"
	"testing"
)

const (
	dx = 150
	dy = 50
)

func TestGenerateImg(t *testing.T) {

	err := readFonts("fonts", ".ttf")
	if err != nil {
		common.Info.Println(err)
		return
	}

	captchaImage := New(dx, dy, RandLightColor())

	captchaImage.DrawNoise(CaptchaComplexLower)

	captchaImage.DrawTextNoise(CaptchaComplexLower)

	captchaImage.DrawText(RandText(4))
	//captchaImage.Drawline(3);
	captchaImage.DrawBorder(ColorToRGB(0x17A7A7A))
	//captchaImage.DrawSineLine()
	captchaImage.DrawHollowLine()

	f, _ := os.Create("capcha.png")
	w := bufio.NewWriter(f)
	captchaImage.SaveImage(w, ImageFormatPng)
	w.Flush()
}
