package ws

// Hub maintains the set of active clients and broadcasts messages to the
// clients.
type Hub struct {
	// Registered clients.
	clients map[*Client]bool

	// Inbound messages from the clients.
	broadcast chan []byte

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client
}

func (h *Hub) GetBroadcast() chan []byte {
	return h.broadcast
}

var defaultHub *Hub
var H = newHub()

func newHub() *Hub {
	if defaultHub != nil {
		return defaultHub
	}
	defaultHub = &Hub{
		broadcast:  make(chan []byte),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[*Client]bool),
	}
	return defaultHub
}

func (h *Hub) Run() {

	//time.AfterFunc(1 * time.Second, func() {
	//	fmt.Println("dida")
	//	for {
	//		h.broadcast <- []byte("n")
	//		time.Sleep(5 * time.Second)
	//	}
	//})

	for {
		select {
		case client := <-h.register:
			h.clients[client] = true
		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				close(client.send)
			}
		case message := <-h.broadcast:
			for client := range h.clients {
				select {
				case client.send <- message:
				default:
					close(client.send)
					delete(h.clients, client)
				}
			}
		}
	}
}
